call ./Stop_PARROT.bat

start .\nginx.exe

pushd .
cd .\aiIntegration
for /f %%i in ('where python') do set LOCALPYTHONPATH=%%i
.\python_environments\python_server\python.exe -m flask --app api_server run --port=3001 
popd