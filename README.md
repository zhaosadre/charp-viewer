<!-- prettier-ignore-start -->
<!-- markdownlint-disable -->
<div align="center">
  <h1>Medical Imaging Local Application</h1>
  <p>This is a zero-footprint medical image local application with the Orthanc server</p>
</div>

<hr />

### Requirements

- [Yarn 1.22.18+](https://yarnpkg.com/en/docs/install)
- [Node 17+](https://nodejs.org/en/)
- [Orthanc 1.8.2] (https://www.orthanc-server.com/)
- Yarn Workspaces should be enabled on your machine:
  - `yarn config set workspaces-experimental true`

### Getting Started (on going)

1. Fork this repository
2. Clone your forked repository
3. Navigate to the cloned project's directory
4. Add this repo as a `remote` named `upstream`
5. `yarn install` to restore dependencies and link projects
6. `yarn run dev`


## Acknowledgments

[OHIF](https://github.com/OHIF)

## License

MIT © Wei Sadre ICTEAM UCLouvain

