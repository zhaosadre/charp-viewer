from api_server.api import bp
from flask import jsonify, request
from api_server.api.errors import bad_request, error_response
import os
import json, uuid
import sys

filepath = "trainedModels/aiModelInfos.json"
backupfilepath = filepath+".bak"
backupfilepath2 = filepath+".bak2"

@bp.route('/modelinfo/store', methods=['POST'])
def saveModelInfo():
    try: 
        # make backup of old model info file
        if os.path.isfile(filepath):
            if os.path.isfile(backupfilepath):
                os.replace(backupfilepath, backupfilepath2)
            os.replace(filepath, backupfilepath)
            
        # write model info
        data = request.get_json() or {}   
        with open(filepath, "w") as outfile:
            json.dump(data, outfile)
                    
            # delete old backup
            if os.path.isfile(backupfilepath2):
                os.remove(backupfilepath2)
                
            response = jsonify({
                "message": 'Saving AI model information was successful'
            })
            response.status_code = 200
            return response
    except Exception as e:
        return error_response(500, "Storing the AI model information failed: "+str(e))


@bp.route('/modelinfo/load', methods=['POST'])
def loadModelInfo():   
    if os.path.isfile(filepath):
        path = filepath
    elif os.path.isfile(backupfilepath):
        path = backupfilepath
    else:
        return jsonify([])
    try:
        with open(path, "r") as f:
            return f.read()
    except Exception as e:
        return error_response(500, "cannot read modelfile")
    