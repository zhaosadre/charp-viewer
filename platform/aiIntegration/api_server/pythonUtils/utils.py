import json
import os
import os.path
import pydicom
import sys
import yaml
import requests
import urllib.request


def createTemporaryDirectory(dirPath):
    isExists = os.path.exists(dirPath)
    if not isExists:
        os.makedirs(dirPath)
        print('Created the temporary directory:', dirPath)
    else: 
        print('Temporary directory exists already')
