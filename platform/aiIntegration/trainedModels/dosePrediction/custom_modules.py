import torch

from torch import nn
from torch.nn.modules import Module, Upsample, LazyConv3d, ReLU
from torch.nn.modules.batchnorm import BatchNorm3d
from torch.nn.modules.instancenorm import InstanceNorm3d
from torch.nn.modules.dropout import Dropout


class DenseConvBlock(Module):
    """Dense convolution block
    """

    def __init__(self, layer_idx, out_channels, kernel_size, dropout_rate, padding):
        """Object creation.

        :param layer_idx: layer identifier.
        :param out_channels: number of channels out.
        :param kernel_size: kernel size.
        :param dropout_rate: dropout rated used.
        :param padding: padding used.
        """
        super(DenseConvBlock, self).__init__()
        self.layer_idx = layer_idx
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.padding = padding

        self.conv3d_1 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu1 = ReLU(inplace=True)

        self.bn_1 = BatchNorm3d(out_channels)
        self.in_1 = InstanceNorm3d(out_channels)
        self.do_1 = Dropout(p=dropout_rate)

        self.conv3d_2 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu2 = ReLU(inplace=True)

        self.bn_2 = BatchNorm3d(out_channels)
        self.in_2 = InstanceNorm3d(out_channels)
        self.do_2 = Dropout(p=dropout_rate)

    def forward(self, inputs, instance_norm_active=True, dropout_active=True):
        """Forward inputs throught the layer

        :param inputs: the input that go throught the layer.
        :param instance_norm_active: boolean in order do instance normalization or not.
        :param dropout_active: boolean in order to know if we need to use dropout.
        :return: the x that was forwarded.
        """
        # We can transform the relu activation function as a submodule used in a sequential allowing hook function
        c1 = self.conv3d_1(inputs)
        c1 = self.relu1(c1)
        if instance_norm_active:
            n1 = self.in_1(c1)
        else:
            n1 = self.bn_1(c1)
        if dropout_active:
            buff1 = self.do_1(n1)
        else:
            buff1 = n1
        buff2 = torch.cat([buff1, inputs], dim=1)

        c2 = self.conv3d_2(buff2)
        c2 = self.relu2(c2)
        if instance_norm_active:
            n2 = self.in_2(c2)
        else:
            n2 = self.bn_2(c2)
        if dropout_active:
            buff3 = self.do_2(n2)
        else:
            buff3 = n2
        x = torch.cat([buff3, buff2], dim=1)

        return x


class ConvBlock(Module):
    """Convulutional block.
    """

    def __init__(self, layer_idx, out_channels, kernel_size, dropout_rate, padding):
        """Object creation.

        :param layer_idx: layer identifier.
        :param out_channels: number of channels out.
        :param kernel_size: kernel size.
        :param dropout_rate: dropout rated used.
        :param padding: padding used.
        """
        super(ConvBlock, self).__init__()
        self.layer_idx = layer_idx
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.padding = padding

        self.conv3d_1 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu1 = ReLU(inplace=True)

        self.bn_1 = BatchNorm3d(out_channels)
        self.in_1 = InstanceNorm3d(out_channels)
        self.do_1 = Dropout(p=dropout_rate)

        self.conv3d_2 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu2 = ReLU(inplace=True)

        self.bn_2 = BatchNorm3d(out_channels)
        self.in_2 = InstanceNorm3d(out_channels)
        self.do_2 = Dropout(p=dropout_rate)

    def forward(self, inputs, instance_norm_active=True, dropout_active=True):
        """Forward inputs throught the layer

        :param inputs: the input that go throught the layer.
        :param instance_norm_active: boolean in order do instance normalization or not.
        :param dropout_active: boolean in order to know if we need to use dropout.
        :return: the x that was forwarded.
        """
        c1 = self.conv3d_1(inputs)
        c1 = self.relu1(c1)
        if instance_norm_active:
            n1 = self.in_1(c1)
        else:
            n1 = self.bn_1(c1)
        if dropout_active:
            buff1 = self.do_1(n1)
        else:
            buff1 = n1

        c2 = self.conv3d_2(buff1)
        c2 = self.relu2(c2)
        if instance_norm_active:
            n2 = self.in_2(c2)
        else:
            n2 = self.bn_2(c2)
        if dropout_active:
            x = self.do_2(n2)
        else:
            x = n2

        return x


class DilateBottleNeckConvBlock(Module):
    """Dilate bottleneck convulutional block.
    """

    def __init__(self, layer_idx, out_channels, kernel_size, dropout_rate, padding, dilation_rate):
        """Object creation.

        :param layer_idx: layer identifier.
        :param out_channels: number of channels out.
        :param kernel_size: kernel size.
        :param dropout_rate: dropout rated used.
        :param padding: padding used.
        :param dilation_rate:
        """
        super(DilateBottleNeckConvBlock, self).__init__()
        self.layer_idx = layer_idx
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.padding = padding
        self.dilation_rate = dilation_rate

        self.conv3d_1 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size,
                                   padding='same', dilation=dilation_rate)
        self.relu1 = ReLU(inplace=True)

        self.bn_1 = BatchNorm3d(out_channels)
        self.in_1 = InstanceNorm3d(out_channels)
        self.do_1 = Dropout(p=dropout_rate)

    def forward(self, inputs, instance_norm_active=True, dropout_active=True):
        """Forward inputs throught the layer

        :param inputs: the input that go throught the layer.
        :param instance_norm_active: boolean in order do instance normalization or not.
        :param dropout_active: boolean in order to know if we need to use dropout.
        :return: the x that was forwarded.
        """
        c1 = self.conv3d_1(inputs)
        c1 = self.relu1(c1)
        if instance_norm_active:
            n1 = self.in_1(c1)
        else:
            n1 = self.bn_1(c1)
        if dropout_active:
            buff1 = self.do_1(n1)
        else:
            buff1 = n1

        return buff1


class BottleNeckConvBlock(Module):
    """Bottleneck convolutional block
    """

    def __init__(self, layer_idx, out_channels, kernel_size, dropout_rate, padding):
        """Object creation.

        :param layer_idx: layer identifier.
        :param out_channels: number of channels out.
        :param kernel_size: kernel size.
        :param dropout_rate: dropout rated used.
        :param padding: padding used.
        """
        super(BottleNeckConvBlock, self).__init__()
        self.layer_idx = layer_idx
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.padding = padding

        self.conv3d_1 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu1 = ReLU(inplace=True)

        self.bn_1 = BatchNorm3d(out_channels)
        self.in_1 = InstanceNorm3d(out_channels)
        self.do_1 = Dropout(p=dropout_rate)

    def forward(self, inputs, instance_norm_active=True, dropout_active=True):
        """Forward inputs throught the layer

        :param inputs: the input that go throught the layer.
        :param instance_norm_active: boolean in order do instance normalization or not.
        :param dropout_active: boolean in order to know if we need to use dropout.
        :return: the x that was forwarded.
        """
        c1 = self.conv3d_1(inputs)
        c1 = self.relu1(c1)
        if instance_norm_active:
            n1 = self.in_1(c1)
        else:
            n1 = self.bn_1(c1)
        if dropout_active:
            buff1 = self.do_1(n1)
        else:
            buff1 = n1

        return buff1


class UpConvBlock(Module):
    """Upsample convulutional block.
    """

    def __init__(self, layer_idx, out_channels, kernel_size, dropout_rate, pool_size, padding):
        """Object creation.

        :param layer_idx: layer identifier.
        :param out_channels: number of channels out.
        :param kernel_size: kernel size.
        :param dropout_rate: dropout rated used.
        :param pool_size: the pool size used.
        :param padding: padding used.
        """
        super(UpConvBlock, self).__init__()
        self.layer_idx = layer_idx
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.pool_size = pool_size
        self.padding = padding

        self.up_1 = Upsample(size=pool_size)
        self.conv3d_1 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu1 = ReLU(inplace=True)

        self.bn_1 = BatchNorm3d(out_channels)
        self.in_1 = InstanceNorm3d(out_channels)
        self.do_1 = Dropout(p=dropout_rate)

        self.conv3d_2 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu2 = ReLU(inplace=True)

        self.bn_2 = BatchNorm3d(out_channels)
        self.in_2 = InstanceNorm3d(out_channels)
        self.do_2 = Dropout(p=dropout_rate)

        self.conv3d_3 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu3 = ReLU(inplace=True)

        self.bn_3 = BatchNorm3d(out_channels)
        self.in_3 = InstanceNorm3d(out_channels)
        self.do_3 = Dropout(p=dropout_rate)

    def forward(self, inputs, skip_input, instance_norm_active=True, dropout_active=True):
        """Forward inputs throught the layer

        :param inputs: the input that go throught the layer.
        :param skip_input: input to skip during the forward
        :param instance_norm_active: boolean in order do instance normalization or not.
        :param dropout_active: boolean in order to know if we need to use dropout.
        :return: the x that was forwarded.
        """
        u1 = self.up_1(inputs)
        c1 = self.conv3d_1(u1)
        c1 = self.relu1(c1)
        if instance_norm_active:
            n1 = self.in_1(c1)
        else:
            n1 = self.bn_1(c1)
        if dropout_active:
            buff1 = self.do_1(n1)
        else:
            buff1 = n1
        buff2 = torch.cat([buff1, skip_input], dim=1)

        c2 = self.conv3d_2(buff2)
        c2 = self.relu2(c2)
        if instance_norm_active:
            n2 = self.in_2(c2)
        else:
            n2 = self.bn_2(c2)
        if dropout_active:
            buff3 = self.do_2(n2)
        else:
            buff3 = n2

        c3 = self.conv3d_3(buff3)
        c3 = self.relu3(c3)
        if instance_norm_active:
            n3 = self.in_3(c3)
        else:
            n3 = self.bn_3(c3)
        if dropout_active:
            x = self.do_3(n3)
        else:
            x = n3

        return x


class DenseUpConvBlock(Module):
    """Dense up convolutional block.
    """

    def __init__(self, layer_idx, out_channels, kernel_size, dropout_rate, pool_size, padding):
        """
        :param layer_idx: layer identifier.
        :param out_channels: number of channels out.
        :param kernel_size: kernel size.
        :param dropout_rate: dropout rated used.
        :param pool_size: the pool size used.
        :param padding: padding used.
        """
        super(DenseUpConvBlock, self).__init__()
        self.layer_idx = layer_idx
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.dropout_rate = dropout_rate
        self.pool_size = pool_size
        self.padding = padding

        self.up_1 = Upsample(scale_factor=pool_size)
        self.conv3d_1 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu1 = ReLU(inplace=True)

        self.bn_1 = BatchNorm3d(out_channels)
        self.in_1 = InstanceNorm3d(out_channels)
        self.do_1 = Dropout(p=dropout_rate)

        self.conv3d_2 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu2 = ReLU(inplace=True)

        self.bn_2 = BatchNorm3d(out_channels)
        self.in_2 = InstanceNorm3d(out_channels)
        self.do_2 = Dropout(p=dropout_rate)

        self.conv3d_3 = LazyConv3d(out_channels=out_channels, kernel_size=kernel_size, padding='same')
        self.relu3 = ReLU(inplace=True)

        self.bn_3 = BatchNorm3d(out_channels)
        self.in_3 = InstanceNorm3d(out_channels)
        self.do_3 = Dropout(p=dropout_rate)

    def forward(self, inputs, skip_input, instance_norm_active=True, dropout_active=True):
        """Forward inputs throught the layer

        :param inputs: the input that go throught the layer.
        :param skip_input: input to skip during the forward
        :param instance_norm_active: boolean in order do instance normalization or not.
        :param dropout_active: boolean in order to know if we need to use dropout.
        :return: the x that was forwarded.
        """
        u1 = self.up_1(inputs)
        c1 = self.conv3d_1(u1)
        c1 = self.relu1(c1)
        if instance_norm_active:
            n1 = self.in_1(c1)
        else:
            n1 = self.bn_1(c1)
        if dropout_active:
            buff1 = self.do_1(n1)
        else:
            buff1 = n1
        buff2 = torch.cat([buff1, skip_input], dim=1)

        c2 = self.conv3d_2(buff2)
        c2 = self.relu2(c2)
        if instance_norm_active:
            n2 = self.in_2(c2)
        else:
            n2 = self.bn_2(c2)
        if dropout_active:
            buff3 = self.do_2(n2)
        else:
            buff3 = n2
        buff4 = torch.cat([buff3, buff2], dim=1)

        c3 = self.conv3d_3(buff4)
        c3 = self.relu3(c3)
        if instance_norm_active:
            n3 = self.in_3(c3)
        else:
            n3 = self.bn_3(c3)
        if dropout_active:
            buff5 = self.do_3(n3)
        else:
            buff5 = n3
        x = torch.cat([buff5, buff4], dim=1)

        return x

    ###############################################################################
    #                              Used in modular model                          #
    ###############################################################################


# TODO : Create Identity layer (when we don't want a dropout for example)
class LazyConvDropoutNormNonlinCat(nn.Module):
    """Lazy Convolutional layer using dropout, normalization, nonlinear activation function and concatenation
    """

    def __init__(self, output_channels, kernel_size=(3, 3, 3), padding='same', norm_type: Module = InstanceNorm3d,
                 dropout_type: Module = Dropout, dropout_rate=0, nonlin: Module = ReLU):
        """Object creation

        :param output_channels: number of output channels.
        :param kernel_size: kernel size, default is (3, 3, 3).
        :param padding: padding used, default is 'same'.
        :param norm_type: normalization type that is used, default is 3D instance normalization. Must be a torch Module.
        :param dropout_type: dropout type that is used, default is Dropout. Must be a torch Module.
        :param dropout_rate: dropout rate used by dropout, default is 0.
        :param nonlin: the nonlinear activation function to use, default is ReLU. Must be a torch Module.
        """
        super(LazyConvDropoutNormNonlinCat, self).__init__()
        self.output_channels = output_channels
        self.kernel_size = kernel_size
        self.padding = padding
        self.norm_type = norm_type(output_channels)
        self.dropout_type = dropout_type
        self.dropout_rate = dropout_rate

        self.nonlin = nonlin()
        self.conv = LazyConv3d(out_channels=output_channels, kernel_size=kernel_size, padding=padding)
        self.dropout = self.dropout_type(p=dropout_rate)

    def forward(self, x, skip=None):
        """Forward inputs through the layer

        :param x: the input to forward.
        :param skip: if skip is not None, it is concatenated with the result of the input thought the layer.
        :return: the input concatenate with its results or the result concatenate with the skip.
        """
        buff = self.conv(x)
        buff = self.nonlin(buff)
        buff = self.norm_type(buff)
        # He is using dropout first in nnunet (maybe investigate)
        buff = self.dropout(buff)
        if skip is not None:
            return torch.cat([buff, skip], dim=1)
        else:
            return torch.cat([buff, x], dim=1)


class ModularConvLayers(Module):
    """Modular convolutional layer
    """

    def __init__(self, output_channels, num_conv_layers=2, kernel_size=(3, 3, 3), padding='same', pool_size=(2, 2, 2),
                 conv_type: Module = LazyConvDropoutNormNonlinCat, norm_type: Module = InstanceNorm3d,
                 dropout_type: Module = Dropout, dropout_rate=0, nonlin: Module = ReLU, upsampling=False):
        """Object creation

        :param output_channels: number of output channels.
        :param num_conv_layers: number of convolutional layers wanted, default is 2.
        :param kernel_size: kernel size, default is (3, 3, 3).
        :param padding: padding used, default is 'same'.
        :param pool_size: only used when upsampling set to True
        :param conv_type: type of convolution used, default is a lazy convolution using:
            - dropout;
            - normalization;
            - nonlinear activation function;
            - concatenation.
        Must be a torch Module (should be a custom Module)
        :param norm_type: normalization type that is used, default is 3D instance normalization. Must be a torch Module.
        :param dropout_type: dropout type that is used, default is Dropout. Must be a torch Module.
        :param dropout_rate: dropout rate used by dropout, default is 0.
        :param nonlin: the nonlinear activation function to use, default is ReLU. Must be a torch Module.
        :param upsampling: boolean that activate or deactivate upsampling. Allowing to do both part of the
            U-shape network.
        """
        super(ModularConvLayers, self).__init__()
        self.output_channels = output_channels
        self.num_conv_layers = num_conv_layers
        self.kernel_size = kernel_size
        self.padding = padding
        self.pool_size = pool_size
        self.conv_type = conv_type
        self.norm_type = norm_type
        self.dropout_type = dropout_type
        self.dropout_rate = dropout_rate
        self.nonlin = nonlin
        self.upsampling = upsampling

        self.blocks = []

        if upsampling:
            self.deconv = Upsample(scale_factor=pool_size)
            self.first_block = conv_type(output_channels=output_channels, kernel_size=kernel_size, padding=padding,
                                         norm_type=norm_type, dropout_type=dropout_type, dropout_rate=dropout_rate,
                                         nonlin=self.nonlin)

        for _ in range(num_conv_layers):
            self.blocks.append(conv_type(output_channels=output_channels, kernel_size=kernel_size, padding=padding,
                                         norm_type=norm_type, dropout_type=dropout_type, dropout_rate=dropout_rate,
                                         nonlin=self.nonlin))
        self.blocks = nn.ModuleList(self.blocks)

    def forward(self, x, skip=None):
        """Forward inputs through the layer

        :param x: the input to forward.
        :param skip: if skip is not None, it is passed to the upsampling part.
        :return: the input concatenate with its results or the result concatenate with the skip.
        """
        if self.upsampling and skip is not None:
            x = self.deconv(x)
            x = self.first_block(x, skip)
        for block in self.blocks:
            x = block(x)
        return x


class LazyConvBottleneckLayer(Module):
    """Lazy Convolutional bottleneck layer using dropout, normalization, nonlinear activation function
    """

    def __init__(self, output_channels, kernel_size=(3, 3, 3), padding='same', norm_type: Module = InstanceNorm3d,
                 dropout_type: Module = Dropout, dropout_rate=0, nonlin: Module = ReLU):
        """Object creation

        :param output_channels: number of output channels.
        :param kernel_size: kernel size, default is (3, 3, 3).
        :param padding: padding used, default is 'same'.
        :param norm_type: normalization type that is used, default is 3D instance normalization. Must be a torch Module.
        :param dropout_type: dropout type that is used, default is Dropout. Must be a torch Module.
        :param dropout_rate: dropout rate used by dropout, default is 0.
        :param nonlin: the nonlinear activation function to use, default is ReLU. Must be a torch Module.
        """
        super(LazyConvBottleneckLayer, self).__init__()
        self.output_channels = output_channels
        self.kernel_size = kernel_size
        self.padding = padding
        self.norm_type = norm_type
        self.dropout_type = dropout_type
        self.dropout_rate = dropout_rate
        self.nonlin = nonlin

        self.block = nn.Sequential(
            LazyConv3d(out_channels=output_channels, kernel_size=kernel_size, padding=padding),
            self.nonlin(), self.norm_type(output_channels), dropout_type(dropout_rate)
        )

    def forward(self, x):
        """Forward inputs through the layer

        :param x: the input to forward.
        :return: the input forwarded through the layer.
        """
        return self.block(x)
