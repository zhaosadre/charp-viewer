import os
import os.path
import sys
import json
import torch


# ++++++++++++++++++++++++++++++++IMPORTANT+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Fixed the issue: sklearn OMP: Error #15 ("Initializing libiomp5md.dll, but found mk2iomp5md.dll already initialized.") 
# when fitting models: 
# https://stackoverflow.com/questions/20554074/sklearn-omp-error-15-initializing-libiomp5md-dll-but-found-mk2iomp5md-dll-a
os.environ["KMP_DUPLICATE_LIB_OK"]="TRUE"


# +++++++++++++ Conversion imports +++++++++++++++++
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(os.path.abspath(".."))
# +++++++++++++ Conversion imports +++++++++++++++++

from dicom_to_nii import convert_ct_struct_dicom_to_nii
from nii_to_dicom import convert_dose_nii_to_dicom

# Import AI model
from modular_hdunet import modular_hdunet
from torch.nn import MaxPool3d, ReLU

# AI MONAI libraries
from monai.networks.nets import UNet
from monai.networks.layers import Norm
from monai.inferers import sliding_window_inference
from monai.data import CacheDataset, DataLoader, Dataset, decollate_batch, NibabelReader
from monai.utils import first
from monai.transforms import (
    EnsureChannelFirstd,
    Compose,
    CropForegroundd,
    ScaleIntensityRanged,
    Invertd,
    AsDiscreted,
    ToTensord,
    ConcatItemsd,
    ThresholdIntensityd,
    RemoveSmallObjectsd,
    KeepLargestConnectedComponentd,
    Activationsd
)
# Preprocessing
from preprocessing import LoadImaged, Separate_structsd
# Postprocessing
from postprocessing import SaveImaged, add_contours_exist
import matplotlib.pyplot as plt
import numpy as np
from utils import *

def predict(tempPath, patient_id, ctSeriesInstanceUID, rtstructInstanceID, runInterpreter, oar_list, tv_list):
    
    # Important: Check the input parameters #################
    if not patient_id or patient_id == "":
        sys.exit("No Patient dataset loaded: Load the patient dataset in Study Management.")
    if not ctSeriesInstanceUID or ctSeriesInstanceUID == "":
        sys.exit("No CT series instance UID to load the CT images. Check for CT data in your study")
    
    print("+++ tempath: ", tempPath)
    print("+++ patient_id: ", patient_id)
    print("+++ CT Series Instance UID: ", ctSeriesInstanceUID)
    print("+++ runInterpreter", runInterpreter)
    
    # model_path = os.path.join('/test','monai_attempt','Code','Dose_prediction_pt','log_no_pk',"proton_dose_prediction_model.pth")
    
    # ROI list
    # oar_list = ['BODY','Brainstem', 'Esophagus_upper', 'GlotticArea', 'OralCavity', 'Parotid_L',
    # 'Parotid_R', 'PharConsInf', 'PharConsMid', 'PharConsSup', 'Submandibular_L', 'Submandibular_R',
    # 'SpinalCord', 'SupraglotLarynx']
    # tv_list = ['CTV_7000', 'CTV_5425']
    
    # Important: Configure path ###########################   
    dir_base = os.path.join(tempPath, patient_id)
    createdir(dir_base)
    dir_ct_dicom = os.path.join(dir_base, 'ct_dicom')
    createdir(dir_ct_dicom)
    dir_ct_nii = os.path.join(dir_base, "ct_nii")
    createdir(dir_ct_nii)
    dir_prediction_nii = os.path.join(dir_base, 'prediction_nii')
    createdir(dir_prediction_nii)
    dir_prediction_dicom = os.path.join(dir_base, 'prediction_dicom')
    createdir(dir_prediction_dicom)

    # Trained model
    model_path = r'best_metric_dose_model.pth'
    if not os.path.exists(model_path):
        sys.exit("Trained model not found")

    print("Loading CT DICOM from Orthanc for", ctSeriesInstanceUID)    
    downloadSeriesInstanceByModality(ctSeriesInstanceUID, dir_ct_dicom, "CT")
    
    print("Loading RTSTRUCT DICOM from Orthanc for", rtstructInstanceID)
    downloadSeriesInstanceByModality(rtstructInstanceID, dir_ct_dicom, "RTSTRUCT")

    # Conversion DICOM to nii
    print("Converting Struct DICOM to nii")
    refCT = convert_ct_struct_dicom_to_nii(dir_dicom = dir_ct_dicom, dir_nii = dir_ct_nii, oar_list = oar_list, tv_list = tv_list, outputnameTV = 'struct_tv.nii.gz', outputnameOar = 'struct_oar.nii.gz', newvoxelsize = None)#[3,3,3])
    
    # Dictionary with patient to predict
    #test_Data = [{'image':os.path.join(dir_ct_dicom_nii,patient_id,'nii_files_3.0_3.0_3.0','ct.nii.gz'),
    #              'tv_together':os.path.join(dir_ct_dicom_nii,patient_id,'nii_files_3.0_3.0_3.0','struct_tv.nii.gz'),
    #              'oar_together':os.path.join(dir_ct_dicom_nii,patient_id,'nii_files_3.0_3.0_3.0','struct_oar.nii.gz'),
    #              'patient_name':patient_id
    #              }]
    test_Data = [{'image':os.path.join(dir_ct_nii,'ct.nii.gz'),
                'tv_together':os.path.join(dir_ct_nii,'struct_tv.nii.gz'),
                'oar_together':os.path.join(dir_ct_nii,'struct_oar.nii.gz')
                }]
    
    # Transformations
    test_pretransforms = Compose(
        [
            LoadImaged(keys=["image",'tv_together','oar_together']),
            EnsureChannelFirstd(keys=["image",'tv_together','oar_together']),
            # ThresholdIntensityd(keys=["image"], threshold=1560, above=False, cval=1560),
            ScaleIntensityRanged(
                keys=["image"], a_min=-1000, a_max=1560,
                b_min=0.0, b_max=1.0, clip=True,
            ),
            Separate_structsd(keys=["oar_together"], numstruct = 13),
            ToTensord(keys=["oar_split"]),
            ConcatItemsd(keys=['image',"tv_together","oar_split"],name="input",dim=0),
            ToTensord(keys=["input","image","tv_together"])
        ]
    )
    test_posttransforms = Compose(
        [
            # Activationsd(keys="pred", softmax=True),
            # Invertd(
            #     keys="pred",  # invert the `pred` data field, also support multiple fields
            #     transform=test_pretransforms,
            #     orig_keys="image",  # get the previously applied pre_transforms information on the `img` data field,
            #                       # then invert `pred` based on this information. we can use same info
            #                       # for multiple fields, also support different orig_keys for different fields
            #     nearest_interp=False,  # don't change the interpolation mode to "nearest" when inverting transforms
            #                            # to ensure a smooth output, then execute `AsDiscreted` transform
            #     to_tensor=True,  # convert to PyTorch Tensor after inverting
            # ),
            # AsDiscreted(keys="pred",  argmax=True, to_onehot=2, threshold=0.5),
            # KeepLargestConnectedComponentd(keys="pred",is_onehot=True),
            SaveImaged(keys="pred_dose", meta_keys="image_meta_dict",  output_postfix="RTDose",separate_folder=False, output_dir=dir_prediction_nii, resample=False)
        ]
    )

    # Define DataLoader using MONAI, CacheDataset needs to be used
    test_ds = CacheDataset(data=test_Data, transform=test_pretransforms, num_workers=0)
    test_loader = DataLoader(test_ds, batch_size=1, shuffle=True, num_workers=0)

    # check_ds = Dataset(data=test_Data, transform=test_pretransforms)
    # check_loader = DataLoader(check_ds, batch_size=1)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    model_param = dict(
                base_num_filter=15,
                num_blocks_per_stage_encoder=2,
                num_stages=4,
                pool_kernel_sizes=((2, 2, 2), (2, 2, 2), (2, 2, 2), (2, 2, 2)),
                conv_kernel_sizes=((3, 3, 3), (3, 3, 3), (3, 3, 3), (3, 3, 3)),
                conv_bottleneck_kernel_sizes=((3, 3, 3), (3, 3, 3), (3, 3, 3), (3, 3, 3)),
                num_blocks_per_stage_decoder=None,
                padding='same',
                num_steps_bottleneck=4,
                expansion_rate=1,
                pooling_type=MaxPool3d,
                pooling_kernel_size=(2, 2, 2),
                nonlin=ReLU
            )

    model = modular_hdunet(**model_param)
    trained_model_dict = torch.load(model_path) if torch.cuda.is_available() else torch.load(model_path, map_location=torch.device('cpu'))
    model.load_state_dict(trained_model_dict['state_dict'])
    del trained_model_dict
    # model.load_state_dict(torch.load(model_path))
    model = model.to(device)
    # print("MODEL",model)

    model.eval()
    with torch.no_grad():
        for d in test_loader:
            images = d["input"].to(device)
            print("d['image'].shape",d["image"].shape,"d['tv_together'].shape",d["tv_together"].shape,"d['oar_split'].shape",d["oar_split"].shape)
            d['pred_dose'] = sliding_window_inference(inputs=images, roi_size=(96,96,64),sw_batch_size=1,  predictor = model)
            print("post dose prediction")
            d['pred_dose'] = [test_posttransforms(i) for i in decollate_batch(d)]        
            print("post save")
            
            # model.cpu()
    
    # predicted files
    predictedNiiFile = os.path.join(dir_prediction_nii, patient_id + '_RTDose.nii.gz')
    predictedDicomFile = os.path.join(dir_prediction_dicom, 'predicted_rtdose.dcm')

    # Conversion DICOM to nii
    convert_dose_nii_to_dicom(dicomctdir = dir_ct_dicom, predictedNiiFile = predictedNiiFile, predictedDicomFile = predictedDicomFile, predicted_structures=['BODY'], refCT = refCT)
    
    print("Conversion nii to DICOM done")

    # Transfer predicted DICOM to Orthanc
    uploadDicomToOrthanc(predictedDicomFile)

    print("Upload predicted result to Orthanc done")
    
    print("Dose prediction done")

'''
Prediction parameters provided by the server. Select the parameters to be used for prediction:
    [1] tempPath: The path where the predict.py is stored,
    [2] patientname: python version,
    [3] ctSeriesInstanceUID: Series instance UID for data set with modality = CT. To predict 'MR' modality data, retrieve the CT UID by the code (see Precision Code)
    [4] rtStructSeriesInstanceUID: Series instance UID for modality = RTSTURCT
    [5] regSeriesInstanceUID: Series instance UID for modality = REG,
    [6] runInterpreter: The python version for the python environment
    [7] oarList: only for dose predciton. For contour predicion oarList = []
    [8] tvList:  only for dose prediction. For contour prediction tvList = []
''' 
if __name__ == '__main__':
    predict(tempPath=sys.argv[1], patient_id=sys.argv[2], ctSeriesInstanceUID=sys.argv[3], rtstructInstanceID=sys.argv[4], runInterpreter=sys.argv[6], oar_list=json.loads(sys.argv[7]), tv_list=json.loads(sys.argv[8]))