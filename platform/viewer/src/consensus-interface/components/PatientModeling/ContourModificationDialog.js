import { Button, Modal } from 'antd';
import React from 'react';
import 'react-confirm-alert/src/react-confirm-alert.css';
import './styles.css';

class ModificationDescriptionStandardTable extends React.Component {
  constructor(props) {
    super(props);

    this.onChangeValue = this.onChangeValue.bind(this);
    this.state = {};
  }

  data = [
    { anatomical: 'Air cavity', addImage: 'Edema', noCompliance: 'Margin error on CTV', noGroup: 'Co-registration errors' },
    { anatomical: 'Bone barrier', addImage: 'Pleural effusion', noCompliance: '', noGroup: 'Other' },
    { anatomical: 'Muscle barrier', addImage: 'Atelectasia', noCompliance: '', noGroup: '' },
    { anatomical: 'Vessel barrier', addImage: 'Tumor localisation error', noCompliance: '', noGroup: '' },
    { anatomical: 'Organ barrier', addImage: 'Fibrosis', noCompliance: '', noGroup: '' },
    { anatomical: 'Bone infiltration', addImage: 'Errors made by DL Model', noCompliance: '', noGroup: '' },
    { anatomical: 'Muscle infiltration', addImage: '', noCompliance: '', noGroup: '' },
    { anatomical: 'Vessel infiltration', addImage: '', noCompliance: '', noGroup: '' },
    { anatomical: 'Organ infiltration', addImage: '', noCompliance: '', noGroup: '' },
    { anatomical: 'Body/External contour', addImage: '', noCompliance: '', noGroup: '' },
  ];

  onChangeValue(event) {
    this.props.setStandardDescriptionValue(event.target.value);
  }

  render() {
    const headStyle = {
      border: 'solid 1px black',
      textAlign: 'center',
      fontSize: '12px',
      padding: '2px',
      backgroundColor: 'grey',
      color: 'white',
    };

    const tdStyle = {
      padding: '1px',
      border: 'solid 1px grey',
      color: 'white',
      fontSize: '14px',
    };

    return (
      <table
        className="contourModDescStandardTable"
        style={{
          border: 'sold 1px grey',
          fontSize: '14px',
          padding: '2px',
          textAlign: 'center',
        }}
      >
        <thead>
          <tr>
            <th className="cModDescAnaTH" rowSpan="1" style={headStyle}>
              Anatomical barriers
            </th>
            <th className="cModDescAddImageTH" rowSpan="1" style={headStyle}>
              Additional Image / <br /> endoscopy information
            </th>
            <th className="cModDescNoComplianceTH" rowSpan="1" style={headStyle}>
              No compliance to <br /> guidelines
            </th>
            <th className="cModDescNoneTH" rowSpan="1" style={headStyle}>
              {' '}
              --
            </th>
          </tr>
        </thead>
        <tbody className="contourModDescStandardTB">
          {this.data.map((item) => (
            <tr className="contourModDesStandardTR">
              <td style={tdStyle}>
                <div className="cModDescAnatomicalBarriersRadio">
                  <input id={item.anatomical} type="radio" value={item.anatomical} name="contourModDescription" onChange={this.onChangeValue} />
                  <label for={item.anatomical} className="cmdDescriptionAnatomicalLabel">
                    {item.anatomical}
                  </label>
                </div>
              </td>
              <td style={tdStyle}>
                <div className="cModDesAdditionalImageDIV">
                  <input id={item.addImage} type="radio" value={item.addImage} name="contourModDescription" onChange={this.onChangeValue} />
                  <label for={item.addImage} className="cmdDescriptionAddImageLabel">
                    {item.addImage}
                  </label>
                </div>
              </td>
              <td style={tdStyle}>
                <div className="cModDesNoComplianceDIV">
                  <input id={item.noCompliance} type="radio" value={item.noCompliance} name="contourModDescription" onChange={this.onChangeValue} />
                  <label for={item.noCompliance} className="cmdDescriptionNoComplianceLabel">
                    {item.noCompliance}
                  </label>
                </div>
              </td>
              <td style={tdStyle}>
                <div className="cModDesOthersDIV">
                  <input id={item.noGroup} type="radio" value={item.noGroup} name="contourModDescription" onChange={this.onChangeValue} />
                  <label for={item.noGroup} className="cmdDescriptionNoGroupLabel">
                    {item.noGroup}
                  </label>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
}

export default class ContourModificationDialog extends React.Component {
  constructor(props) {
    super(props);
    props.contourModificationFunction.setVisible = (isVisible) => this.setVisible(isVisible);
    this.onLabelTextValueChange = this.onLabelTextValueChange.bind(this);
    this.onOtherDescriptionValueChange = this.onOtherDescriptionValueChange.bind(this);

    this.state = {
      visible: false,
      labelText: '',
      otherDescription: '',
      description: '',
      showLabelErrorMessage: false,
      showDescriptionErrorMessage: false,
      showOtherDescriptionErrorMessage: false,
      activeOtherDescription: false,
    };
  }

  componentDidUpdate(prevProps) {
    this.props.contourModificationFunction.setVisible = (isVisible) => this.setVisible(isVisible);
  }

  setVisible(isVisible) {
    this.setState({
      visible: isVisible,
    });
  }

  isOtherDescription() {
    if (this.state.description == 'Other' && this.state.otherDescription.trim() == '') {
      return false;
    }
    return true;
  }

  handleOK() {
    if (this.state.description.trim() != '' && this.isOtherDescription() && this.state.labelText.trim() != '') {
      // Get date time
      var today = new Date();
      const date =
        today.getFullYear() +
        '-' +
        (today.getMonth() + 1) +
        '-' +
        today.getDate() +
        '  ' +
        today.getHours() +
        ':' +
        today.getMinutes() +
        ':' +
        today.getSeconds();

      const modification = {
        label: this.state.labelText,
        description: this.state.description == 'Other' ? this.state.otherDescription : this.state.description,
        contourID: this.props.selectedContour.getID(),
        time: date,
      };
      this.props.onDialogOK(modification);
      this.setVisible(false);
      this.setState({
        labelText: '',
        description: '',
        otherDescription: '',
        showDescriptionErrorMessage: false,
        showLabelErrorMessage: false,
        showOtherDescriptionErrorMessage: false,
        activeOtherDescription: false,
      });
    } else {
      if (this.state.description.trim() == '') {
        this.setState({ showDescriptionErrorMessage: true });
      }
      if (this.state.labelText.trim() == '') {
        this.setState({ showLabelErrorMessage: true });
      }
      if (this.state.description == 'Other' && this.state.otherDescription.trim() == '') {
        this.setState({ showOtherDescriptionErrorMessage: true });
      }
    }
  }

  handleCancel() {
    this.setState({
      labelText: '',
      description: '',
      otherDescription: '',
      showLabelErrorMessage: false,
      showDescriptionErrorMessage: false,
      showOtherDescriptionErrorMessage: false,
      activeOtherDescription: false,
    });
    this.setVisible(false);
  }

  onLabelTextValueChange(event) {
    this.setState({
      labelText: event.target.value,
    });
  }

  onOtherDescriptionValueChange(event) {
    this.setState({
      otherDescription: event.target.value,
    });
  }

  setStandardDescriptionValue(description) {
    if (description) {
      this.setState({ description: description });
      if (description.trim() == 'Other') {
        this.setState({
          activeOtherDescription: true,
          showLabelErrorMessage: false,
          showDescriptionErrorMessage: false,
          showOtherDescriptionErrorMessage: false,
        });
      } else {
        this.setState({
          activeOtherDescription: false,
          showLabelErrorMessage: false,
          showDescriptionErrorMessage: false,
          showOtherDescriptionErrorMessage: false,
        });
      }
    }
  }

  renderOtherDescription() {
    return this.state.activeOtherDescription ? (
      <tr className="enterOtherDescriptionTR">
        <td className="enterOtherDescription" style={{ width: '120px' }}>
          Other Description:
        </td>
        <td>
          <input
            className="contorModificationEntreOtherDescriptionInput"
            type="text"
            id="inputContourModDesOtherDescriptionId"
            size="40"
            onChange={(event) => this.onOtherDescriptionValueChange(event)}
            value={this.state.otherDescription}
          />
        </td>
      </tr>
    ) : null;
  }

  renderErrorMessage() {
    let message = 'Please set the LABEL TEXt, DESCRIPTION or OTHER DESCRIPTION for the modification';
    return this.state.showDescriptionErrorMessage || this.state.showLabelErrorMessage || this.state.showOtherDescriptionErrorMessage ? (
      <tr className="showErrorMessageTR">
        <td className="messageLabel" style={{ width: '120px' }}>
          Message:
        </td>
        <td className="messageText">
          <h3 className="showMessageH3" style={{ color: 'red', paddingTop: '5px', fontSize: '14px' }}>
            {message}
          </h3>
        </td>
      </tr>
    ) : null;
  }

  renderContourNameAndLabelText() {
    return (
      <div>
        <h2 className="contourModificationTitle">Contour Modification</h2>
        <table className="contourModificationGeneralTable">
          <tbody className="contourModificationGeneralTBody">
            <tr className="contourNameTR">
              <td className="contourNameLabel" style={{ width: '120px' }}>
                Contour Name:
              </td>
              <td
                className="contourNameText"
                style={{
                  fontSize: '13px',
                  color: '#F28227',
                  marginLeft: '2px',
                }}
              >
                {this.props.selectedContour?.getFullName()}
              </td>
            </tr>
            <tr className="enterLabelTextTR">
              <td className="enterLabelTextLabel" style={{ width: '120px' }}>
                Label Text:
              </td>
              <td>
                <input
                  className="contorModificationEntreLabelTextInput"
                  type="text"
                  id="inputContourModDesLabelTextId"
                  size="40"
                  onChange={(event) => this.onLabelTextValueChange(event)}
                  value={this.state.labelText}
                />
              </td>
            </tr>
            {this.renderOtherDescription()}
            {this.renderErrorMessage()}
          </tbody>
        </table>
      </div>
    );
  }

  renderContourModDescriptionStandard() {
    return (
      <div>
        <div className="contourModDesStandardLabelDiv">
          <h3 className="cmDescriptionStandarizationLabel">Standardization of labels for contour modification description:</h3>
        </div>
        <div className="contourModificationDescriptionTableDiv">
          <ModificationDescriptionStandardTable
            setStandardDescriptionValue={(description) => this.setStandardDescriptionValue(description)}
          ></ModificationDescriptionStandardTable>
        </div>
      </div>
    );
  }

  render() {
    return (
      <Modal
        className="contourModificationDescriptionModal"
        open={this.state.visible}
        destroyOnClose={true}
        onOk={() => this.handleOK()}
        onCancel={() => this.handleCancel()}
        footer={[
          <Button key="cancel" onClick={() => this.handleCancel()}>
            Cancel
          </Button>,
          <Button key="ok" type="primary" onClick={() => this.handleOK()}>
            OK
          </Button>,
        ]}
      >
        {this.renderContourNameAndLabelText()}
        {this.renderContourModDescriptionStandard()}
      </Modal>
    );
  }
}
