import React from 'react';
import PropTypes from 'prop-types';

// ########### DEFINITIVE ################
// The configuration for PARROT. These numbers come from a protocol established in the Netherlands
const modelparams_definitive_xero_grade2 = {
  beta0: -2,
  beta_Parotid: 0.0906,
  beta_Subm: 0.0182,
  beta_baseline: [0, 0.495, 1.207],
};

const modelparams_definitive_xero_grade3 = {
  beta0: -3.7286,
  beta_Parotid: 0.0855,
  beta_Subm: 0.0156,
  beta_baseline: [0, 0.4249, 1.0361],
};

const modelparams_definitive_dysf_grade2 = {
  beta0: -4.0536,
  beta_Coral: 0.03,
  beta_PCMSup: 0.0236,
  beta_PCMMed: 0.0095,
  beta_PCMInf: 0.0133,
  beta_baseline: [0, 0.9382, 1.29],
  beta_location: [0, -0.6281, -0.7711],
};

const modelparams_definitive_dysf_grade3 = {
  beta0: -7.6174,
  beta_Coral: 0.0259,
  beta_PCMSup: 0.0203,
  beta_PCMMed: 0.0303,
  beta_PCMInf: 0.0341,
  beta_baseline: [0, 0.5738, 1.4718],
  beta_location: [0, 0.0387, -0.5303],
};

/**
 * baseline_score: 0 not at all (EORTC QLQ-H&N35 Q41 score 1), 1 a bit (score 2), 2 moderate-severe (score 3-4)
 * @param {*} baseline_score: in CHARP, we consider severe side-effects so the baseline_score is 2
 * @param {*} Dmean_ParL: mean dose in the left parotid
 * @param {*} Dmean_ParR: mean dose in the right parotid
 * @param {*} Dmean_SubmL: mean dose in the left submandibular gland
 * @param {*} Dmean_SubmR: mean dose in the right submandibular gland
 * @param {*} Volume_SubmL: Volume in voxels or cc of the left submandibular gland
 * @param {*} Volume_SubmR: Volume in voxels or cc of the right submandibular gland
 * @returns the NTCP value for xero grade 2
 */
function NTCP_definitive_xero_grade2(baseline_score, Dmean_ParL, Dmean_ParR, Dmean_SubmL, Dmean_SubmR, Volume_SubmL, Volume_SubmR) {
  let beta = {};
  beta['Constant'] = modelparams_definitive_xero_grade2['beta0'] + modelparams_definitive_xero_grade2['beta_baseline'][baseline_score];
  beta['Parotid'] = modelparams_definitive_xero_grade2['beta_Parotid'];
  beta['Subm'] = modelparams_definitive_xero_grade2['beta_Subm'];

  const Dmean_SubmComb = (Dmean_SubmL * Volume_SubmL + Dmean_SubmR * Volume_SubmR) / (Volume_SubmL + Volume_SubmR);
  const Dmean_ParComb = Math.sqrt(Dmean_ParL) + Math.sqrt(Dmean_ParR);

  const S = beta['Constant'] + beta['Parotid'] * Dmean_ParComb + beta['Subm'] * Dmean_SubmComb;

  const ntcp = 1 / (1 + Math.exp(-S)); // to do check the exp javascript for array
  return ntcp;
}

/**
 * baseline_score: 0 not at all (EORTC QLQ-H&N35 Q41 score 1), 1 a bit (score 2), 2 moderate-severe (score 3-4)
 */
function NTCP_definitive_xero_grade3(baseline_score, Dmean_ParL, Dmean_ParR, Dmean_SubmL, Dmean_SubmR, Volume_SubmL, Volume_SubmR) {
  let beta = {};
  beta['Constant'] = modelparams_definitive_xero_grade3['beta0'] + modelparams_definitive_xero_grade3['beta_baseline'][baseline_score];
  beta['Parotid'] = modelparams_definitive_xero_grade3['beta_Parotid'];
  beta['Subm'] = modelparams_definitive_xero_grade3['beta_Subm'];

  const Dmean_SubmComb = (Dmean_SubmL * Volume_SubmL + Dmean_SubmR * Volume_SubmR) / (Volume_SubmL + Volume_SubmR);
  const Dmean_ParComb = Math.sqrt(Dmean_ParL) + Math.sqrt(Dmean_ParR);

  const S = beta['Constant'] + beta['Parotid'] * Dmean_ParComb + beta['Subm'] * Dmean_SubmComb;

  const ntcp = 1 / (1 + Math.exp(-S)); // to do check exp for array
  return ntcp;
}

/**
 * Input: baseline_score: in CHARP, we consider severe side-effects so the baseline_score is 2
 */
function NTCP_definitive_dysf_grade2(baseline_score, Dmean_Coral, Dmean_PCMSup, Dmean_PCMMed, Dmean_PCMInf, location) {
  let beta = {};
  beta['Constant'] =
    modelparams_definitive_dysf_grade2['beta0'] +
    modelparams_definitive_dysf_grade2['beta_baseline'][baseline_score] +
    modelparams_definitive_dysf_grade2['beta_location'][location];
  beta['Coral'] = modelparams_definitive_dysf_grade2['beta_Coral'];
  beta['PCMSup'] = modelparams_definitive_dysf_grade2['beta_PCMSup'];
  beta['PCMMed'] = modelparams_definitive_dysf_grade2['beta_PCMMed'];
  beta['PCMInf'] = modelparams_definitive_dysf_grade2['beta_PCMInf'];

  const S = beta['Constant'] + beta['Coral'] * Dmean_Coral + beta['PCMSup'] * Dmean_PCMSup + beta['PCMMed'] * Dmean_PCMMed + beta['PCMInf'] * Dmean_PCMInf;

  const ntcp = 1 / (1 + Math.exp(-S)); // check the Math.exp and np.exp
  return ntcp;
}

/**
 * baseline_score: 0 grade 0-I, 1 grade II, 2 grade III-IV
 * location: 0 oral cavity, 1 farynx, 2 larynx
 * baseline_score: in CHARP, we consider severe side-effects so the baseline_score is 2
 */
function NTCP_definitive_dysf_grade3(baseline_score, Dmean_Coral, Dmean_PCMSup, Dmean_PCMMed, Dmean_PCMInf, location) {
  let beta = {};
  beta['Constant'] =
    modelparams_definitive_dysf_grade3['beta0'] +
    modelparams_definitive_dysf_grade3['beta_baseline'][baseline_score] +
    modelparams_definitive_dysf_grade3['beta_location'][location];
  beta['Coral'] = modelparams_definitive_dysf_grade3['beta_Coral'];
  beta['PCMSup'] = modelparams_definitive_dysf_grade3['beta_PCMSup'];
  beta['PCMMed'] = modelparams_definitive_dysf_grade3['beta_PCMMed'];
  beta['PCMInf'] = modelparams_definitive_dysf_grade3['beta_PCMInf'];
  const S = beta['Constant'] + beta['Coral'] * Dmean_Coral + beta['PCMSup'] * Dmean_PCMSup + beta['PCMMed'] * Dmean_PCMMed + beta['PCMInf'] * Dmean_PCMInf;

  const ntcp = 1 / (1 + Math.exp(-S)); // check the Math.exp and np.exp
  return ntcp;
}

/**
 * Location: 0: Oral Cavity, 1: Pharynx, 2: Larynx
 */

// To do
function ntcp_xero_2(allDVHs, doseNumber, doseScaleFactor, parL, parR, submL, submR, baseline_score) {
  // get mean dvh values of segments
  const meanParL = allDVHs[parL.index][doseNumber].mean * doseScaleFactor;
  const meanParR = allDVHs[parR.index][doseNumber].mean * doseScaleFactor;
  const meansubmL = allDVHs[submL.index][doseNumber].mean * doseScaleFactor;
  const meansubmR = allDVHs[submR.index][doseNumber].mean * doseScaleFactor;
  const volumeSubmL = submL.segValue.getSegVolume();
  const volumeSubmR = submR.segValue.getSegVolume();
  return NTCP_definitive_xero_grade2(baseline_score, meanParL, meanParR, meansubmL, meansubmR, volumeSubmL, volumeSubmR);
}

function ntcp_xero_3(allDVHs, doseNumber, doseScaleFactor, parL, parR, submL, submR, baseline_score) {
  // get mean dvh value of segements
  const meanParL = allDVHs[parL.index][doseNumber].mean * doseScaleFactor;
  const meanParR = allDVHs[parR.index][doseNumber].mean * doseScaleFactor;
  const meansubmL = allDVHs[submL.index][doseNumber].mean * doseScaleFactor;
  const meansubmR = allDVHs[submR.index][doseNumber].mean * doseScaleFactor;
  const volumeSubmL = submL.segValue.getSegVolume();
  const volumeSubmR = submR.segValue.getSegVolume();
  return NTCP_definitive_xero_grade3(baseline_score, meanParL, meanParR, meansubmL, meansubmR, volumeSubmL, volumeSubmR);
}

function ntcp_dysf_2(allDVHs, doseNumber, doseScaleFactor, oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location) {
  // get mean dvh value of segements
  const meanOralCavity = allDVHs[oral_cavity.index][doseNumber].mean * doseScaleFactor;
  const meanPcmSup = allDVHs[pcm_sup.index][doseNumber].mean * doseScaleFactor;
  const meanPcmMed = allDVHs[pcm_med.index][doseNumber].mean * doseScaleFactor;
  const meanPcmInf = allDVHs[pcm_inf.index][doseNumber].mean * doseScaleFactor;
  return NTCP_definitive_dysf_grade2(baseline_score, meanOralCavity, meanPcmSup, meanPcmMed, meanPcmInf, location);
}

function ntcp_dysf_3(allDVHs, doseNumber, doseScaleFactor, oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location) {
  // get mean dvh of segements
  const meanOralCavity = allDVHs[oral_cavity.index][doseNumber].mean * doseScaleFactor;
  const meanPcmSup = allDVHs[pcm_sup.index][doseNumber].mean * doseScaleFactor;
  const meanPcmMed = allDVHs[pcm_med.index][doseNumber].mean * doseScaleFactor;
  const meanPcmInf = allDVHs[pcm_inf.index][doseNumber].mean * doseScaleFactor;
  return NTCP_definitive_dysf_grade3(baseline_score, meanOralCavity, meanPcmSup, meanPcmMed, meanPcmInf, location);
}

function calculation_delta_ntcp_xero_2(allDVHs, doseScaleFactor, parL, parR, submL, submR, baseline_score) {
  return (
    ntcp_xero_2(allDVHs, 0, doseScaleFactor[0], parL, parR, submL, submR, baseline_score) -
    ntcp_xero_2(allDVHs, 1, doseScaleFactor[1], parL, parR, submL, submR, baseline_score)
  );
}

function calculation_delta_ntcp_xero_3(allDVHs, doseScaleFactor, parL, parR, submL, submR, baseline_score) {
  return (
    ntcp_xero_3(allDVHs, 0, doseScaleFactor[0], parL, parR, submL, submR, baseline_score) -
    ntcp_xero_3(allDVHs, 1, doseScaleFactor[1], parL, parR, submL, submR, baseline_score)
  );
}

function calculation_delta_ntcp_dysf_2(allDVHs, doseScaleFactor, oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location) {
  return (
    ntcp_dysf_2(allDVHs, 0, doseScaleFactor[0], oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location) -
    ntcp_dysf_2(allDVHs, 1, doseScaleFactor[1], oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location)
  );
}

function calculation_delta_ntcP_dysf_3(allDVHs, doseScaleFactor, oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location) {
  return (
    ntcp_dysf_3(allDVHs, 0, doseScaleFactor[0], oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location) -
    ntcp_dysf_3(allDVHs, 1, doseScaleFactor[1], oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location)
  );
}

function checkNtcpConclusion(xeroGradeValue, dysGradeValue, grade) {
  var result = 'False';
  const sum = xeroGradeValue + dysGradeValue;
  if (grade === 2) {
    if (sum >= 0.15 && xeroGradeValue >= 0.05 && dysGradeValue >= 0.05) {
      result = 'True';
    }
  } else if (grade === 3) {
    if (sum >= 0.075 && xeroGradeValue >= 0.025 && dysGradeValue >= 0.025) {
      result = 'True';
    }
  }
  return result;
}

// Define the data line color based on the value
function checkDataLineColor(value, threshold) {
  return value >= threshold ? '#29C5F6' : '#FFFF33';
}

function findSegment(segs, ntcpContourMapping, segLabel) {
  if (ntcpContourMapping && ntcpContourMapping[segLabel]) {
    const segmentName = ntcpContourMapping[segLabel].segment.toUpperCase();
    var segIndex = segs.findIndex((seg) => seg.getLabel().toUpperCase() === segmentName);
    var segValue = segs[segIndex];
    return { index: segIndex, segValue: segValue };
  } else {
    return { index: -1, segValue: null };
  }
}

function ntcpCalculation(allDVHs, segs, doseScaleFactor, baseline_score, location, ntcpContourMapping) {
  var ntcpData = [];

  const parL = findSegment(segs, ntcpContourMapping, 'Parotid Left'); //'PAROTID_L'
  const parR = findSegment(segs, ntcpContourMapping, 'Parotid Right'); // 'PAROTID_R'
  const submR = findSegment(segs, ntcpContourMapping, 'Submandibular Gland Right'); //'SUBMANDIBULAR_R'
  const submL = findSegment(segs, ntcpContourMapping, 'Submandibular Gland Left'); //'SUBMANDIBULAR_L'
  const oral_cavity = findSegment(segs, ntcpContourMapping, 'Oral Cavity'); //'ORALCAVITY'
  const pcm_sup = findSegment(segs, ntcpContourMapping, 'Pharconstructor Muscle Sup'); //'PHARCONSSUP'
  const pcm_med = findSegment(segs, ntcpContourMapping, 'Pharconstructor Muscle Middle'); //'PHARCONSMID'
  const pcm_inf = findSegment(segs, ntcpContourMapping, 'Pharconstructor Muscle Inferior'); //'PHARCONSINF'

  if (
    parL.index !== -1 &&
    parL.segValue &&
    parR.index !== -1 &&
    parR.segValue &&
    submL.index !== -1 &&
    submL.segValue &&
    submR.index !== -1 &&
    submR.segValue &&
    oral_cavity.index !== -1 &&
    oral_cavity.segValue &&
    pcm_sup.index !== -1 &&
    pcm_sup.segValue &&
    pcm_med.index !== -1 &&
    pcm_med.segValue &&
    pcm_inf.index !== -1 &&
    pcm_inf.segValue
  ) {
    const deltaXero2 = calculation_delta_ntcp_xero_2(allDVHs, doseScaleFactor, parL, parR, submL, submR, baseline_score);
    const deltaXero3 = calculation_delta_ntcp_xero_3(allDVHs, doseScaleFactor, parL, parR, submL, submR, baseline_score);
    const deltaDysf2 = calculation_delta_ntcp_dysf_2(allDVHs, doseScaleFactor, oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location);
    const deltaDysf3 = calculation_delta_ntcP_dysf_3(allDVHs, doseScaleFactor, oral_cavity, pcm_sup, pcm_med, pcm_inf, baseline_score, location);

    const conclusionGrade2 = checkNtcpConclusion(deltaXero2, deltaDysf2, 2);
    const deltaXero2Color = checkDataLineColor(deltaXero2, 0.1);
    const deltaDysf2Color = checkDataLineColor(deltaDysf2, 0.1);

    const conclusionGrade3 = checkNtcpConclusion(deltaXero3, deltaDysf3, 3);
    const deltaXero3Color = checkDataLineColor(deltaXero3, 0.05);
    const deltaDysf3Color = checkDataLineColor(deltaDysf2, 0.05);

    ntcpData.push({
      criteria: '\u0394NTCP Xerostomia grade >= II >= 10%',
      value: deltaXero2 ? (deltaXero2 * 100).toFixed(2) + '%' : '---',
      color: deltaXero2Color,
    });
    ntcpData.push({
      criteria: '\u0394NTCP Xerostomia grade >= III >= 5%',
      value: deltaXero3 ? (deltaXero3 * 100).toFixed(2) + '%' : '---',
      color: deltaXero3Color,
    });
    ntcpData.push({
      criteria: '\u0394NTCP Dysphagia grade >= II >= 10%',
      value: deltaDysf2 ? (deltaDysf2 * 100).toFixed(2) + '%' : '---',
      color: deltaDysf2Color,
    });
    ntcpData.push({
      criteria: '\u0394NTCP Dysphagia grade >= III >= 5%',
      value: deltaDysf3 ? (deltaDysf3 * 100).toFixed(2) + '%' : '---',
      color: deltaDysf3Color,
    });
    ntcpData.push({
      criteria: 'Sum\u0394NTCP grade >= II >= 15% and each >= 5%',
      value: conclusionGrade2,
      color: '#FFFF33',
    });
    ntcpData.push({
      criteria: 'Sum\u0394NTCP grade >= III >= 7.5% and each >= 2.5%',
      value: conclusionGrade3,
      color: '#FFFF33',
    });
  }
  return ntcpData;
}

function getMessageBackgroundColor(ntcp) {
  var color = '#FFFF33';
  ntcp.map((item) => {
    if (item.color === '#29C5F6') {
      color = item.color;
    }
  });
  return color;
}

export default function NTCPLippv22({ segDataSet, doseScaleFactor, baseline_score, location, ntcpContourMapping, allDVHs }) {
  NTCPLippv22.propTypes = {
    doseScaleFactor: PropTypes.number.isRequired,
    baseline_score: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    segDataSet: PropTypes.object.isRequired,
    allDVHs: PropTypes.object.isRequired,
    ntcpContourMapping: PropTypes.object.isRequired,
    ntcpMappingErrorMessage: PropTypes.string,
  };

  const segs = segDataSet.getSegs();
  const ntcp = ntcpCalculation(allDVHs, segs, doseScaleFactor, baseline_score, location, ntcpContourMapping);
  let messageColor = getMessageBackgroundColor(ntcp);

  if (ntcp && ntcp.length > 0) {
    const result = (
      <div className="ntcpLippResultTableDiv">
        <table
          className="ntcpLippv22ResultTable"
          style={{
            border: 'solid 1px grey',
            fontSize: '14px',
            padding: '2px',
            textAlign: 'çenter',
          }}
        >
          <thead className="ntcpLippResultTableHeader">
            <tr className="ntcpModelHeaderTR">
              <th
                className="ntcpModelCriteriaHeader"
                style={{
                  border: 'solid 1px black',
                  textAlign: 'center',
                  fontSize: '12px',
                  padding: '2px',
                  backgroundColor: 'grey',
                  color: 'white',
                }}
              >
                NTCP model criteria:
              </th>
              <th
                className="ntcpValueHeader"
                style={{
                  border: 'solid 1px black',
                  textAlign: 'center',
                  fontSize: '12px',
                  padding: '2px',
                  backgroundColor: 'grey',
                  color: 'white',
                }}
              >
                Value:
              </th>
            </tr>
          </thead>
          <tbody className="ntcpResultTableTBody">
            {ntcp.map((item) => (
              <tr className="ntcpResultTableTR">
                <td
                  className="ntcpResultTableCriteriaTD"
                  style={{
                    padding: '1px',
                    border: 'solid 1px grey',
                    color: item.color,
                    fontSize: '12px',
                    textAlign: 'left',
                  }}
                >
                  <div className="ntcpResultTableCriteriaDiv">
                    <span
                      className="ntcpResultTableCriteriaSpan"
                      style={{
                        textAlign: 'left',
                        fontSize: '13px',
                        padding: '1px',
                      }}
                    >
                      {item.criteria}
                    </span>
                  </div>
                </td>
                <td
                  className="ntcpResultTableValueTD"
                  style={{
                    padding: '1px',
                    border: 'solid 1px grey',
                    color: item.color,
                    fontSize: '13px',
                    textAlign: 'center',
                  }}
                >
                  {item.value}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <div
          className="ntcpLippResultMessageDiv"
          style={{
            padding: '1px',
            border: 'solid 1px grey',
            backgroundColor: messageColor,
            fontSize: '13px',
            textAlign: 'center',
            margin: '5px',
            color: 'black',
            whiteSpace: 'pre-wrap',
          }}
        >
          {messageColor === '#29C5F6'
            ? 'This patient is selected for a PT treatment according to' + '\nLandelijk Indication Protocol Protontherapy (LIPPv2.2)'
            : 'This patient is selected for a XT treatment according to' + '\nLandelijk Indication Protocol Protontherapy (LIPPv2.2)'}
        </div>
      </div>
    );
    return result;
  } else {
    return (
      <h3 className="ntcpMessage" style={{ color: 'red', textAlign: 'center', paddingTop: '80px' }}>
        NTCP computation not available. Check the contour mapping!
      </h3>
    );
  }
}
