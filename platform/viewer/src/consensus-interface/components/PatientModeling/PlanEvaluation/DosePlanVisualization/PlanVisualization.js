import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import DVHGraphic from './DVHGraphic';
import { Icon } from '../../../../utils/icons/Icon';
import DVHNormalizationDialog from './DVHNormalizationDialog';
import ClinicalGoalsLoadParseDialog from './ClinicalGoalsLoadParseDialog';
import DoseStatisticTable from './DoseStatisticTable';
import ClinicalGoalsTable from './ClinicalGoalsTable';
import NTCPLippv22 from './NTCPLippv22';
import NTCPEsophagusScripture from './NTCPEsophagusScripture';
import NTCPConfigDialog from './NTCPConfigDialog';
import { LinearProgress } from '@mui/material';
import PropTypes from 'prop-types';
import '../../styles.css';

// ************ api ****************
async function saveNtcpSettings(ntcpSettings) {
  const response = await fetch('aimodels/systemInfo/ntcpSettings/store', {
    method: 'POST',
    body: JSON.stringify(ntcpSettings),
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.status == 200) {
    const data = (await response.json()).message;
    return data;
  } else {
    const text = await response.text();
    var error = text;
    try {
      error = JSON.parse(text).message;
    } catch {}
    throw error;
  }
}

async function loadNtcpSettings() {
  const response = await fetch('aimodels/systemInfo/ntcpSettings/load', {
    method: 'POST',
    body: '{}',
    headers: {
      'Content-Type': 'application/json',
    },
  });
  if (response.status == 200) {
    const data = await response.json();
    return data;
  } else {
    const text = await response.text();
    var error = text;
    try {
      error = JSON.parse(text).message;
    } catch {}
    throw error;
  }
}
// ********************************************************************

export default class PlanVisualization extends React.Component {
  constructor(props) {
    super(props);

    PlanVisualization.propTypes = {
      doseScaleFactor: PropTypes.array,
      secondarySeriesInstanceUID: PropTypes.string,
      SeriesInstanceUID: PropTypes.string,
      segDataSet: PropTypes.object,
      doseIndex1: PropTypes.number,
      doseIndex2: PropTypes.number,
      getDoses: PropTypes.func,
      doseDataSet: PropTypes.object,
      onDoseScaleFactor: PropTypes.func,
    };

    this.state = {
      dvhNormalizationSetting: {
        prescriptionValue: 56.0,
        selectedContour: null,
        selectedDVHMetric: 'Dmean',
      },
      selectedNtcpModelName: null,
      ntcpSettings: [
        {
          modelName: 'ntcpLIPPv22',
          ntcpParameterSetting: {
            baselineScore: '0 grade 0-I',
            location: '0=oral cavity',
            ntcpContourMapping: {},
          },
        },
        {
          modelName: 'ntcpEsophagusStricture',
          ntcpParameterSetting: {
            ntcpContourMapping: {},
          },
        },
      ],
      isNtcpSettingLoaded: false,
      clinicalGoalsTableData: [],
      doseScaleFactor: props.doseScaleFactor,
      cgLoadedFileName: null,
      canRenderPlanVisualization: false,
    };

    this.dvhSettingFunctions = {};
    this.clinicalGoalsLoadParseFunction = {};
    this.ntcpSettingsFunctions = {};
    this.allDVHs = null;
    this.dialogKey = 0;
  }

  // DVH statistics must be recalculated if segment dataset changes
  static getDerivedStateFromProps(props, state) {
    return props.segDataSet == state.segDataSet
      ? null
      : {
          segDataSet: props.segDataSet,
          dvhCalculationStatus: null,
        };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.secondarySeriesInstanceUID !== nextProps.secondarySeriesInstanceUID ||
      this.props.SeriesInstanceUID !== nextProps.SeriesInstanceUID ||
      this.state.dvhNormalizationSetting !== nextState.dvhNormalizationSetting ||
      this.state.clinicalGoalsTableData !== nextState.clinicalGoalsTableData ||
      this.state.selectedNtcpModelName !== nextState.selectedNtcpModelName ||
      this.state.ntcpSettings !== nextState.ntcpSettings ||
      this.state.dvhCalculationStatus !== nextState.dvhCalculationStatus ||
      this.props.segDataSet !== nextProps.segDataSet ||
      this.props.doseIndex1 !== nextProps.doseIndex1 ||
      this.props.doseIndex2 !== nextProps.doseIndex2
    );
  }

  componentDidMount() {
    this.isComputeDVHCanceled = false;
  }

  componentWillUnmount() {
    this.isComputeDVHCanceled = true;
  }

  // Get dose value by the volume percent value
  getDoseByVolume(dvh, volumes) {
    const result = volumes.map((volume) => {
      // find the volume y[i] in the DVH (y axis) that is the closest
      // and less or equal to "volume"
      var i = 0;
      while (i < dvh.y.length && dvh.y[i] > volume) {
        i++;
      }
      // if the value is not the first value of the DVH, we also take
      // the larger volume value y[i-1] before it and do a linear interpolation
      // to find the dosis for "volume".
      if (i !== 0) {
        const x0 = dvh.x[i - 1];
        const y0 = dvh.y[i - 1];
        const x1 = dvh.x[i];
        const y1 = dvh.y[i];
        return x0 + ((volume - y0) * (x1 - x0)) / (y1 - y0);
      } else {
        return dvh.x[0];
      }
    });
    return result;
  }

  // get volume value by dose value
  getVolumeByDose(dvh, doses) {
    const result = doses.map((dose) => {
      // find the dose x[i] in the DVH (x axis) that is the closest
      // and greater or equal to "dose"
      var i = 0;
      while (i < dvh.x.length && dvh.x[i] < dose) {
        i++;
      }
      // if the value is not the first value of the DVH, we also take
      // the smaller dose value x[i-1] before it and do a linear interpolation
      // to find the volume for "dose".
      if (i !== 0) {
        const x0 = dvh.x[i - 1];
        const y0 = dvh.y[i - 1];
        const x1 = dvh.x[i];
        const y1 = dvh.y[i];
        return y0 + ((dose - x0) * (y1 - y0)) / (x1 - x0);
      } else {
        return dvh.y[0];
      }
    });
    return result;
  }

  /*
      metric: the metric, for example "D95", "D90" or "Dmean"
      refNorm: reference dose to normalize
  */
  getNormalizationScaleFactor(dose, seg, metric, refNorm) {
    const dvh = dose.getDVH(seg, 800);

    // check whether the metric has the form "Dn" where "n" is a number between 0 and 100
    const volume = parseFloat(metric.substring(1));
    const cKey = metric[0];
    if (cKey === 'D') {
      if (!isNaN(volume) && volume >= 0 && volume <= 100) {
        return refNorm / this.getDoseByVolume(dvh, [volume])[0];
      } else if (metric.substring(1) === 'mean') {
        return refNorm / dvh.mean;
      } else if (metric.substring(1) === 'max') {
        return refNorm / dvh.max;
      }
    }
    // not supported metric (e.g. V95)
    return 1.0;
  }

  calculateDoseScaleFactor(settings) {
    const doses = this.props.doseDataSet.getDoses();
    const scaleFactors = doses.map((dose) => {
      if (settings.selectedContour == null) {
        return 1.0;
      } else {
        return this.getNormalizationScaleFactor(dose, settings.selectedContour, settings.selectedDVHMetric, settings.prescriptionValue);
      }
    });
    return scaleFactors;
  }

  dvhSettingToText() {
    const setting = this.state.dvhNormalizationSetting;
    if (setting.selectedContour == null) {
      return '(no normalization)';
    } else {
      return 'normalized ' + setting.selectedDVHMetric + ' of ' + setting.selectedContour.getLabel() + ' to ' + setting.prescriptionValue + 'Gy';
    }
  }

  nctpSetModelNameLabel() {
    if (this.state.selectedNtcpModelName == null) {
      return '(no NTCP model selected)';
    } else {
      return 'Model Name: ' + this.state.selectedNtcpModelName;
    }
  }

  ntcpSetClinicalGoalFileNameLabel() {
    if (this.state.cgLoadedFileName == null) {
      return '(Select the clinical goal excel file)';
    } else {
      return 'File Name: ' + this.state.cgLoadedFileName;
    }
  }
  /*
  computeAllDVHs() {
    this.allDVHs = [];
    const segs = this.props.segDataSet.getSegs();
    const doses = this.props.doseDataSet.getDoses();
    for (let s = 0; s < segs.length; s++) {
      this.allDVHs[s] = [];
      for (let d = 0; d < doses.length; d++) {
        this.allDVHs[s][d] = doses[d].getDVH(segs[s], 800);
      }
    }
  }
  */

  computeDVHForSegment(s, segs, doses, callbackDone, callbackError) {
    try {
      this.allDVHs[s] = [];
      for (let d = 0; d < doses.length; d++) {
        if (this.isComputeDVHCanceled) {
          return;
        }
        this.allDVHs[s][d] = doses[d].getDVH(segs[s], 800);
      }
      s++;
      if (s < segs.length) {
        setTimeout(() => {
          this.computeDVHForSegment(s, segs, doses, callbackDone, callbackError);
        }, 0);
      } else {
        callbackDone();
      }
    } catch (error) {
      callbackError(error);
    }
  }

  computeAllDVHs(callbackDone, callbackError) {
    this.allDVHs = [];
    const segs = this.props.segDataSet.getSegs();
    const doses = this.props.doseDataSet.getDoses();
    setTimeout(() => {
      this.computeDVHForSegment(0, segs, doses, callbackDone, callbackError);
    }, 0);
  }

  // Get doses statistic data for the dose statistic table
  getDoseStatistics() {
    const segs = this.props.segDataSet.getSegs();
    const doses = this.props.doseDataSet.getDoses();
    var result = [];
    for (let s = 0; s < segs.length; s++) {
      for (let d = 0; d < doses.length; d++) {
        const dvh = this.allDVHs[s][d];
        const sf = this.state.doseScaleFactor[d];
        const doseByVolume = this.getDoseByVolume(dvh, [99, 98, 95, 50, 5, 2]);
        const volumeByDose = this.getVolumeByDose(dvh, [40 / sf, 30 / sf, 20 / sf, 10 / sf, 5 / sf]);
        const item = {
          general: {
            doseName: doses[d].getLabel(),
            roi: segs[s].getLabel(),
            color: segs[s].getColor(),
            roiVol: (segs[s].getSegVolume() / 1000.0).toFixed(2),
          },
          dose: {
            doseD99: (doseByVolume[0] * sf).toFixed(2),
            doseD98: (doseByVolume[1] * sf).toFixed(2),
            doseD95: (doseByVolume[2] * sf).toFixed(2),
            average: (dvh.mean * sf).toFixed(2),
            doseD50: (doseByVolume[3] * sf).toFixed(2),
            doseD5: (doseByVolume[4] * sf).toFixed(2),
            doseD2: (doseByVolume[5] * sf).toFixed(2),
          },
          volume: {
            volumeV40Gy: volumeByDose[0].toFixed(2),
            volumeV30Gy: volumeByDose[1].toFixed(2),
            volumeV20Gy: volumeByDose[2].toFixed(2),
            volumeV10Gy: volumeByDose[3].toFixed(2),
            volumeV5Gy: volumeByDose[4].toFixed(2),
          },
        };
        result.push(item);
      }
    }
    return result;
  }

  // parses the Gy value from a parameter string
  // and returns it. Accepted units are "Gy" or "cGy".
  // returns NaN if unit is wrong or param does not
  // contain a number.
  parseGyValue(param, unit) {
    var value = Number(param);
    if (unit === 'Gy') {
      return value;
    } else if (unit === 'cGy') {
      return value / 100.0;
    } else {
      return NaN;
    }
  }

  // parses the percent value from a parameter string
  // and returns it. Accepted unit is "%".
  // returns NaN if unit is wrong or param does not
  // contain a number.
  parsePercentValue(param, unit) {
    var value = Number(param);
    if (unit === '%') {
      return value;
    } else {
      return NaN;
    }
  }

  // parses the volume value from a parameter string
  // and returns it in cm^3. Accepted unit is "cc".
  // returns NaN if unit is wrong or param does not
  // contain a number.
  parseVolumeValue(param, unit) {
    const value = Number(param);
    if (unit === 'cc') {
      return value;
    } else {
      return NaN;
    }
  }

  // returns true if the value satisfies the goal.
  // returns false otherwise or if error (unknown operator)
  checkGoal(actualValue, goalValue, operator) {
    if (operator == '<') {
      return actualValue < goalValue;
    } else if (operator == '<=') {
      return actualValue <= goalValue;
    } else if (operator == '>') {
      return actualValue > goalValue;
    } else if (operator == '>=') {
      return actualValue >= goalValue;
    } else {
      return false;
    }
  }

  // Create the clinical goals dataset for the table
  getClinicalGoalsTableData(goals) {
    const segs = this.props.segDataSet.getSegs();
    const allDoses = this.props.doseDataSet.getDoses();
    const doses = [allDoses[this.props.doseIndex1], allDoses[this.props.doseIndex1]];

    var result = [];
    for (let s = 0; s < segs.length; s++) {
      const segName = segs[s].getLabel().toUpperCase();
      for (let g = 0; g < goals.length; g++) {
        const goal = goals[g];
        if (goal.key && segName === goal.key.toUpperCase()) {
          const evaluatedGoal = {
            priority: goal.priority,
            roiPoi: segs[s].getLabel(),
            color: segs[s].getColor(),
            goal: goal.goal,
            values: [],
            passed: [],
          };

          const goalType = goal.type.toUpperCase();
          if (goalType === 'DMEAN') {
            const goalGy = this.parseGyValue(goal.param1, goal.unit1);
            if (!Number.isNaN(goalGy)) {
              for (let d = 0; d < doses.length; d++) {
                const dvh = this.allDVHs[s][d];
                const sf = this.state.doseScaleFactor[d];
                const mean = dvh.mean * sf;
                evaluatedGoal.values.push(mean.toFixed(2) + 'Gy');
                evaluatedGoal.passed.push(this.checkGoal(mean, goalGy, goal.operator));
              }
            }
          } else if (goalType === 'DXGY IN %') {
            const goalGy = this.parseGyValue(goal.param1, goal.unit1);
            const percentValue = this.parsePercentValue(goal.param2, goal.unit2);
            if (!Number.isNaN(goalGy) && !Number.isNaN(percentValue)) {
              for (let d = 0; d < doses.length; d++) {
                const dvh = this.allDVHs[s][d];
                const sf = this.state.doseScaleFactor[d];
                const actualGy = this.getDoseByVolume(dvh, [percentValue])[0] * sf;
                evaluatedGoal.values.push(actualGy.toFixed(2) + 'Gy');
                evaluatedGoal.passed.push(this.checkGoal(actualGy, goalGy, goal.operator));
              }
            }
          } else if (goalType === 'VX(%)GY') {
            const goalPercent = this.parsePercentValue(goal.param1, goal.unit1);
            const gyValue = this.parseGyValue(goal.param2, goal.unit2);
            if (!Number.isNaN(goalPercent) && !Number.isNaN(gyValue)) {
              for (let d = 0; d < doses.length; d++) {
                const dvh = this.allDVHs[s][d];
                const sf = this.state.doseScaleFactor[d];
                const actualPercent = this.getVolumeByDose(dvh, [gyValue / sf])[0];
                evaluatedGoal.values.push(actualPercent.toFixed(2) + '%');
                evaluatedGoal.passed.push(this.checkGoal(actualPercent, goalPercent, goal.operator));
              }
            }
          } else if (goalType === 'DXGY IN CC') {
            const goalGy = this.parseGyValue(goal.param1, goal.unit1);
            const volumeValue = this.parseVolumeValue(goal.param2, goal.unit2); // in cm^3
            if (!Number.isNaN(goalGy) && !Number.isNaN(volumeValue)) {
              // calculate the volume percentage from the volume
              const totalRoiVolume = segs[s].getSegVolume() / 1000.0; // in cm^3
              const percentValue = (volumeValue / totalRoiVolume) * 100.0; // in percents
              // this is the same code as for 'DXGY IN %':
              for (let d = 0; d < doses.length; d++) {
                const dvh = this.allDVHs[s][d];
                const sf = this.state.doseScaleFactor[d];
                const actualGy = this.getDoseByVolume(dvh, [percentValue])[0] * sf;
                evaluatedGoal.values.push(actualGy.toFixed(2) + 'Gy');
                evaluatedGoal.passed.push(this.checkGoal(actualGy, goalGy, goal.operator));
              }
            }
          }
          result.push(evaluatedGoal);
        }
      }
    }
    return result;
  }

  // Extract the clinical goals data from the file
  extractCalculateClinicalGoalsData(sourceData) {
    var result = [];
    const data = sourceData.slice(4);
    if (data && data.length > 0) {
      for (var i = 0; i < data.length; i++) {
        var dt = data[i];
        result.push({
          key: dt[0], // ROI name
          type: dt[1],
          operator: dt[2],
          param1: dt[3],
          unit1: dt[4],
          param2: dt[5],
          unit2: dt[6],
          priority: dt[7],
          goal: dt[9],
        });
      }
    }
    return result;
  }

  getNtcpModelLippLocatioValue(parameterSetting) {
    const location = parameterSetting.location;
    if (location === '0=oral cavity') {
      return 0;
    } else if (location === '1-pharynx') {
      return 1;
    } else {
      return 2;
    }
  }

  getNtcpModelLippBaselineScoreValue(parameterSetting) {
    const baselineScore = parameterSetting.baselineScore;
    if (baselineScore === '0 grade 0-I') {
      return 0;
    } else if (baselineScore === '1 grade II') {
      return 1;
    } else {
      return 2;
    }
  }

  renderNTCPModel() {
    const modelName = this.state.selectedNtcpModelName;
    const parameterSetting = this.state.ntcpSettings.find((s) => s.modelName === modelName)?.ntcpParameterSetting;

    if (modelName && modelName.toUpperCase() === 'NTCPLIPPV22') {
      return (
        <div className="ntcpLiPPResultTableDiv">
          <NTCPLippv22
            segDataSet={this.props.segDataSet}
            doseScaleFactor={this.state.doseScaleFactor}
            baseline_score={this.getNtcpModelLippBaselineScoreValue(parameterSetting)}
            location={this.getNtcpModelLippLocatioValue(parameterSetting)}
            ntcpContourMapping={parameterSetting.ntcpContourMapping}
            allDVHs={this.allDVHs}
          />
        </div>
      );
    } else if (modelName && modelName.toUpperCase() === 'NTCPESOPHAGUSSTRICTURE') {
      return (
        <div className="ntcpEsophagusResultTableDiv">
          <NTCPEsophagusScripture
            doseDataSet={this.props.doseDataSet}
            segDataSet={this.props.segDataSet}
            doseIndex1={this.props.doseIndex1}
            doseIndex2={this.props.doseIndex2}
            ntcpContourMapping={parameterSetting.ntcpContourMapping}
          />
        </div>
      );
    } else {
      return <div />;
    }
  }

  renderDosePlan(isTop, doseStatistics, goalsData) {
    return (
      <div className={isTop ? 'dosePlanValDivTop' : 'dosePlanValDivBottom'}>
        <Tabs>
          <TabList>
            <Tab>DVH</Tab>
            <Tab>Dose Statistics</Tab>
            <Tab>Clinical goals</Tab>
            <Tab>NTCP</Tab>
          </TabList>
          <TabPanel className="tabDvh">
            <div className="dvhScalingDiv">
              <div className="leftDiv">
                <ul className="leftUl">
                  <li className="leftLi">
                    <button className="dvhSettingButtonDiv" onClick={() => this.dvhSettingFunctions.setVisible(true)}>
                      <Icon className="smallIcon" name="setting"></Icon>
                    </button>
                    <label className="dvhScalingLabel">{this.dvhSettingToText()}</label>
                  </li>
                </ul>
              </div>
              <div className="rightDiv">
                <ul className="rightUl">
                  <li className="rightLi">
                    <label className="dvhLegendLabelDash">- - - - - - Dose 1</label>
                    <label className="dvhLegendLabelSolide">
                      <hr style={{ width: '34px', color: '#F28227', marginRight: '4px' }}></hr>
                      <text style={{ fontSize: '12px', color: '#F28227' }}>Dose 2</text>
                    </label>
                  </li>
                </ul>
              </div>
            </div>
            <DVHGraphic
              doseDataSet={this.props.doseDataSet}
              doseIndex1={this.props.doseIndex1}
              doseIndex2={this.props.doseIndex2}
              segDataSet={this.props.segDataSet}
              doseScaleFactor={this.state.doseScaleFactor}
            />
          </TabPanel>
          <TabPanel className="tabDoseStatistic">
            <div className="doseStatisticTableDiv">
              <DoseStatisticTable className="doseStatisticTable" data={doseStatistics}></DoseStatisticTable>
            </div>
          </TabPanel>
          <TabPanel className="tabClinicalGoals">
            <div className="cgLoadFileDiv">
              <button className="cgLoadingButtonDiv" onClick={() => this.clinicalGoalsLoadParseFunction.setVisible(true)}>
                Import Clinical Goals
              </button>
              <label className="selectClinicalGoalLabel">{this.ntcpSetClinicalGoalFileNameLabel()}</label>
            </div>
            {goalsData && goalsData.length > 0 ? (
              <div className="clinicalGoalsTableDiv">
                <ClinicalGoalsTable className="clinicalGoalsTable" data={goalsData}></ClinicalGoalsTable>
              </div>
            ) : (
              <label
                style={{
                  color: 'red',
                  textAlign: 'center',
                  marginTop: '20px',
                  width: '95%',
                  fontSize: '18px',
                }}
              >
                Warning: If a clinical goal does not appear as expected, please check if the contour names correspond to the clinical goals excel file.
              </label>
            )}
          </TabPanel>
          <TabPanel className="tabNTCP">
            <div className="nctpConfigDiv">
              <button className="ntcpSetConfigButtonDiv" onClick={() => this.ntcpSettingsFunctions.setVisible(true)}>
                Select NTCP Model
              </button>
              <label className="ntcpSelectedModelLabel">{this.nctpSetModelNameLabel()}</label>
            </div>
            {this.allDVHs && this.allDVHs.length > 0 && this.allDVHs[0].length > 1 ? (
              this.renderNTCPModel()
            ) : (
              <label
                style={{
                  color: 'red',
                  textAlign: 'center',
                  marginTop: '20px',
                  width: '95%',
                  fontSize: '18px',
                }}
              >
                NTCP model: Cannot calculate NTCP model since it requires two dose datasets!
              </label>
            )}
          </TabPanel>
        </Tabs>
      </div>
    );
  }

  render() {
    if (!this.props.SeriesInstanceUID) {
      return null;
    }

    if (!this.props.segDataSet) {
      return (
        <div>
          <div>No Contour Data: Can not calculate plan evaluation</div>
        </div>
      );
    }
    if (!this.props.doseDataSet) {
      return (
        <div>
          <div>No Dose Data: Can not calculate plan evaluation</div>
        </div>
      );
    }

    if (!this.state.dvhCalculationStatus) {
      this.computeAllDVHs(
        () => {
          this.setState({ dvhCalculationStatus: 'Done' });
        },
        (error) => {
          this.setState({ dvhCalculationStatus: 'Failed', dvhCalculationError: error });
        }
      );
      return (
        <div className="calculatingDoseStatistisStatusDiv">
          <h3>Calculating dose statistics...</h3>
          <LinearProgress className="circularProgress"></LinearProgress>
        </div>
      );
    } else if (this.state.dvhCalculationStatus === 'Failed') {
      return (
        <div>
          <div>Dose information could not be calculated.</div>
          <div>{this.state.dvhCalculationError.toString()}</div>
        </div>
      );
    }

    // const timeMeas1 = performance.now();
    const doseStatistics = this.getDoseStatistics();
    // Debug the performance
    // const timeMeas2 = performance.now() - timeMeas1;
    // console.log('*** doseStatistics calculated in ' + timeMeas2);

    const goalsData = this.getClinicalGoalsTableData(this.state.clinicalGoalsTableData);

    const planViewTop = this.renderDosePlan(true, doseStatistics, goalsData);
    const planViewBottom = this.renderDosePlan(false, doseStatistics, goalsData);

    // we have to do this here because the DVH calculation doesn't work well if interrupted
    if (!this.state.isNtcpSettingLoaded) {
      loadNtcpSettings()
        .then((loadedNtcpSettings) =>
          this.setState({
            // keep default settings if loaded settings is empty
            ntcpSettings: loadedNtcpSettings.length > 0 ? loadedNtcpSettings : this.state.ntcpSettings,
            isNtcpSettingLoaded: true,
          })
        )
        .catch((error) => this.setState({ ntcpMappingErrorMessage: error }));
    }

    // this forces to recreate the DVHNormalizationDialog every time
    this.dialogKey += 3;

    return (
      <div className="dosePlanValDiv">
        {planViewTop}
        {planViewBottom}
        <DVHNormalizationDialog
          className="dvhNormalizationDialog"
          key={this.dialogKey}
          segDataSet={this.props.segDataSet}
          dvhSettingFunctions={this.dvhSettingFunctions}
          dvhNormalizationSetting={this.state.dvhNormalizationSetting}
          onDvhSettingChanged={(newDvhNormalizationSetting) => {
            const newDoseScaleFactor = this.calculateDoseScaleFactor(newDvhNormalizationSetting);
            this.setState({
              dvhNormalizationSetting: newDvhNormalizationSetting,
              doseScaleFactor: newDoseScaleFactor,
            });
            this.props.onDoseScaleFactor(newDoseScaleFactor);
          }}
        />
        <ClinicalGoalsLoadParseDialog
          className="clinicalGoalsLoadParseDialog"
          key={this.dialogKey + 1}
          clinicalGoalsLoadParseFunction={this.clinicalGoalsLoadParseFunction}
          onGoalsFileLoaded={(data) => {
            const newClinicalData = this.extractCalculateClinicalGoalsData(data);
            this.setState({
              clinicalGoalsTableData: newClinicalData,
            });
          }}
          setClinicalGoalFileName={(fileName) => {
            this.setState({
              cgLoadedFileName: fileName,
            });
          }}
        />
        <NTCPConfigDialog
          className="ntcpSelectModelConfigParametersDialog"
          key={this.dialogKey + 2}
          segDataSet={this.props.segDataSet}
          doseDataSet={this.props.doseDataSet}
          ntcpSettingsFunctions={this.ntcpSettingsFunctions}
          selectedNtcpModelName={this.state.selectedNtcpModelName}
          ntcpSettings={this.state.ntcpSettings}
          onNtcpSettingsChanged={(newSelectedNtcpModelName, newNtcpSettings) => {
            this.setState({
              selectedNtcpModelName: newSelectedNtcpModelName,
              ntcpSettings: newNtcpSettings,
            });
            saveNtcpSettings(newNtcpSettings);
          }}
        />
      </div>
    );
  }
}
