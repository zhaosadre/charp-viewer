import React from 'react';
import PropTypes from 'prop-types';
import Plot from 'react-plotly.js';
import '../../../../data/DoseDataSet';

const plotLayout = {
  title: {
    text: 'DVH',
    font: {
      color: '#FFFFFF',
      size: 10,
    },
  },
  autosize: true,
  showlegend: false,
  pad: 4,
  xaxis: {
    title: 'Dose [Gy]',
    color: '#FFFFFF',
    automargin: true,
    titlefont: { size: 10 },
    showgrid: true,
    zeroline: false,
    showline: true,
    gridwidth: 1,
    linecolor: '#636363',
    linewidth: 1,
    rangemode: 'tozero',
    hoverformat: ',',
    autotick: false,
    dtick: 10,
    ticklen: 4,
    tickwidth: 2,
    tickcolor: 'grey',
    range: [0, 100],
  },
  yaxis: {
    title: 'Volume',
    color: '#FFFFFF',
    dtick: 10,
    automargin: true,
    titlefont: { size: 10 },
    showgrid: true,
    zeroline: false,
    showline: true,
    gridwidth: 1,
    linecolor: '#636363',
    linewidth: 1,
    ticksuffix: '%',
    type: 'linear',
    range: [0, 100],
    hoverformat: ',',
    autotick: false,
    ticklen: 4,
    tickwidth: 2,
    tickcolor: 'grey',
  },
  margin: {
    l: 2,
    r: 2,
    b: 2,
    t: 25,
    pad: 1,
  },
  paper_bgcolor: '#000000',
  plot_bgcolor: '#000000',
  datarevsion: 0,
};

export default class DVHGraphic extends React.Component {
  constructor(props) {
    super(props);
    this.segListenerIds = [];
    this.state = {
      plotData: [],
    };
  }

  static propTypes = {
    segDataSet: PropTypes.object,
    doseDataSet: PropTypes.object,
    doseIndex1: PropTypes.number,
    doseIndex2: PropTypes.number,
    doseScaleFactor: PropTypes.array,
  };

  registerSegmentListeners(segDataSet) {
    if (segDataSet) {
      const segs = segDataSet.getSegs();
      segs.forEach((seg, index) => {
        const listnerID = seg.addListener((whatChange) => {
          this.onSegChange(index, whatChange);
        });
        this.segListenerIds.push(listnerID);
        // if segment already visible, show it in the graph
        if (seg.getVisible()) {
          this.onSegChange(index, 'visible');
        }
      });
    }
  }

  componentDidMount() {
    this.registerSegmentListeners(this.props.segDataSet);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.doseIndex1 !== prevProps.doseIndex1 ||
      this.props.doseIndex2 !== prevProps.doseIndex2 ||
      this.props.segDataSet != prevProps.segDataSet ||
      this.props.doseDataSet != prevProps.doseDataSet ||
      this.props.doseScaleFactor != prevProps.doseScaleFactor
    ) {
      if (this.props.segDataSet != prevProps.segDataSet) {
        this.unregisterSegmentListeners(prevProps.segDataSet);
        this.registerSegmentListeners(this.props.segDataSet);
      }
      this.redoPlot();
    }
  }

  unregisterSegmentListeners(segDataSet) {
    if (segDataSet) {
      const segs = segDataSet.getSegs();
      segs.forEach((seg, index) => {
        this.segListenerIds.forEach((id) => {
          seg.removeListener(id);
        });
      });
      this.segListenerIds = [];
    }
  }

  componentWillUnmount() {
    this.unregisterSegmentListeners(this.props.segDataSet);
  }

  onSegChange(segIndex, whatChange) {
    switch (whatChange) {
      case 'color':
        this.redoPlot();
        break;
      case 'visible':
        this.redoPlot();
        break;
      case 'active':
        // nothing to do
        break;
      case 'data':
        this.redoPlot();
        break;
    }
  }

  getPlotData(hist, doseName, doseIndex) {
    const label = hist.seg.getLabel();
    const color = hist.seg.getColor();
    const doseLabel = doseIndex + 1 + ': ' + doseName;

    // scale x axis
    const scaledDoseValues = hist.x.map((v) => v * this.props.doseScaleFactor[doseIndex]);

    return {
      x: scaledDoseValues,
      y: hist.y,
      mode: 'lines',
      hovertemplate: 'Dose ' + doseLabel + '<br>Name: ' + label + '<br><b>Dose</b>: %{x:.2f}Gy<br>' + '<b>Volume</b>: %{y:.2f}%' + '<extra></extra>',
      line: {
        dash: doseIndex === 1 ? 'solid' : 'dot',
        color: 'rgb(' + color[0] + ',' + color[1] + ',' + color[2] + ')',
      },
    };
  }

  redoPlot() {
    const doses = this.props.doseDataSet?.getDoses();
    const doseName1 = this.props.doseIndex1 !== null ? doses[this.props.doseIndex1].getLabel() : '';
    const doseName2 = this.props.doseIndex2 !== null ? doses[this.props.doseIndex2].getLabel() : '';

    const plotData = [];
    this.props.segDataSet
      .getSegs()
      .filter((segment) => segment.getVisible())
      .forEach((segment) => {
        if (this.props.doseIndex1 !== null) {
          const dvh = doses[this.props.doseIndex1].getDVH(segment, 800);
          plotData.push(this.getPlotData(dvh, doseName1, 0));
        }
        if (this.props.doseIndex2 !== null) {
          const dvh = doses[this.props.doseIndex2].getDVH(segment, 800);
          plotData.push(this.getPlotData(dvh, doseName2, 1));
        }
      });

    this.setState({
      plotData: plotData,
    });
  }

  render() {
    return (
      <div className="dvhPlotDiv">
        <Plot
          data={this.state.plotData}
          layout={plotLayout}
          useResizeHandler={true}
          config={{ displaylogo: false, displayModeBar: false }}
          style={{ width: '100%', height: '100%', marginRight: '10px' }}
        />
      </div>
    );
  }
}
