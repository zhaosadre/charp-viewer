import React from 'react';
import { Dose } from '../../../../data/DoseDataSet';
import PropTypes from 'prop-types';

// _SCALAR_ - the alpha/beta ratio for the LQ model (in Gy)
const abrNominal = 3;
const sNominal = 0.1;

const gammaNominal = 1.4;
const TD50Nominal = 61.5;

function findSegment(segs, ntcpContourMapping, segLabel) {
  if (ntcpContourMapping && ntcpContourMapping[segLabel]) {
    const segmentName = ntcpContourMapping[segLabel].segment.toUpperCase();
    var segIndex = segs.findIndex((seg) => seg.getLabel().toUpperCase() === segmentName);
    var segValue = segs[segIndex];
    return { index: segIndex, segValue: segValue };
  } else {
    return { index: -1, segValue: null };
  }
}

/*
function effectiveDose(doseData, doseFrac, doseRef, abr) {
  const doseEffective = Float32Array(doseData.length)
  const factor = (doseFrac+abr) / (doseRef+abr)
  for(let i=0;i<doseData.length;i++) {
    doseEffective[i] = doseData[i] * factor
  }
  return doseEffective
}
*/

function relativeSeriality(doseData) {
  let doseTot = 0.0;
  for (let i = 0; i < doseData.length; i++) {
    if (doseData[i] > doseTot) {
      // calculate maximum dose doseTot
      doseTot = doseData[i];
    }
  }

  const doseFrac = doseTot / 35;
  const doseRef = 2.0;
  const effectiveDoseFactor = (doseFrac + abrNominal) / (doseRef + abrNominal);

  const v = 1.0 / doseData.length;

  let prod_d = 1.0;
  for (let i = 0; i < doseData.length; i++) {
    const expo = Math.exp(Math.E * gammaNominal * (1 - (doseData[i] * effectiveDoseFactor) / TD50Nominal));
    const prob = Math.pow(2, -expo);
    prod_d *= Math.pow(1 - Math.pow(prob, sNominal), v);
  }
  const ntcp = Math.pow(1 - prod_d, 1 / sNominal);
  return ntcp;
}

function esophagus_stricture_3_mons(dose, segSupraglotLarynx, segPharconssup) {
  const segSupraglotLarynxData = segSupraglotLarynx.getData();
  const segPharconssupData = segPharconssup.getData();

  // make a fake segment that is the disjunction of the two segments.
  // this new segment will be used as the "mask" for the calculation.
  const maskData = new Uint8Array(segSupraglotLarynxData.length);
  for (let i = 0; i < segSupraglotLarynxData.length; i++) {
    maskData[i] = segSupraglotLarynxData[i] | segPharconssupData[i];
  }
  const seg = {
    metaData: {
      ImagePositionPatient: segSupraglotLarynx.metaData.ImagePositionPatient,
      Slices: segSupraglotLarynx.metaData.Slices,
      Rows: segSupraglotLarynx.metaData.Rows,
      Columns: segSupraglotLarynx.metaData.Columns,
      PixelSpacing: segSupraglotLarynx.metaData.PixelSpacing,
      SliceThickness: segSupraglotLarynx.metaData.SliceThickness,
    },
    getData: () => maskData,
  };

  // get dose regridded to the same grid as the segment.
  // note that Z direction of segment is inverted.
  const regriddedDose = dose.getRegriddedDose(
    [
      seg.metaData.ImagePositionPatient[0],
      seg.metaData.ImagePositionPatient[1],
      seg.metaData.ImagePositionPatient[2] - seg.metaData.Slices * seg.metaData.SliceThickness,
    ],
    seg.metaData.PixelSpacing[0],
    seg.metaData.PixelSpacing[1],
    seg.metaData.SliceThickness
  );

  // get the dose values for the voxels in the segment
  const doseValues = Dose.getDoseInSegment(regriddedDose, seg);

  const ntcp = relativeSeriality(doseValues);
  return ntcp;
}

export default function NTCPEsophagusScripture({ doseDataSet, segDataSet, doseIndex1, doseIndex2, ntcpContourMapping }) {
  NTCPEsophagusScripture.propTypes = {
    doseDataSet: PropTypes.object,
    segDataSet: PropTypes.object,
    ntcps: PropTypes.arrayOf(PropTypes.object),
    doseIndex1: PropTypes.number,
    doseIndex2: PropTypes.number,
    ntcpContourMapping: PropTypes.object.isRequired,
  };

  const segs = segDataSet.getSegs();
  const doses = doseDataSet.getDoses();

  // TO DO: find the segments
  const segSupraglotLarynx = findSegment(segs, ntcpContourMapping, 'Pharconstructor Muscle Inferior'); //Pharconstructor Muscle Inferior
  const segPharconssup = findSegment(segs, ntcpContourMapping, 'SUPRAGLOTLARYNX'); //SUPRAGLOTLARYNX

  if (segSupraglotLarynx.index !== -1 && segPharconssup.index !== -1 && segPharconssup.segValue && segSupraglotLarynx.segValue) {
    const ntcpValueDose1 = esophagus_stricture_3_mons(doses[doseIndex1], segSupraglotLarynx.segValue, segPharconssup.segValue);
    const ntcpValueDose2 = esophagus_stricture_3_mons(doses[doseIndex2], segSupraglotLarynx.segValue, segPharconssup.segValue);

    const ntcps = [
      { criteria: 'Probability for a patient to have esophagus stricture (dose PT)', value: (ntcpValueDose1 * 100).toFixed(2).toString() + '%' },
      { criteria: 'Probability for a patient to have esophagus stricture (dose XT)', value: (ntcpValueDose2 * 100).toFixed(2).toString() + '%' },
    ];

    const result = (
      <div className="ntcpEsophagusResultTableDiv">
        <table
          className="ntcpEsophagusResultTable"
          style={{
            border: 'solid 1px grey',
            fontSize: '14px',
            padding: '2px',
            textAlign: 'center',
          }}
        >
          <thead className="ntcpEsophagusModelResultTableHeader">
            <tr className="ntcpEsophagusResultTableTR">
              <th
                className="ntcpEsophagusResultCriterialTH"
                style={{
                  border: 'solid 1px black',
                  textAlign: 'center',
                  fontSize: '12px',
                  padding: '2px',
                  backgroundColor: 'grey',
                  color: 'white',
                }}
              >
                NTCP model criteria:
              </th>
              <th
                className="ntcpEsophagusResultValueTH"
                style={{
                  border: 'solid 1px black',
                  textAlign: 'center',
                  fontSize: '12px',
                  padding: '2px',
                  backgroundColor: 'grey',
                  color: 'white',
                }}
              >
                Value:
              </th>
            </tr>
          </thead>
          <tbody className="ntcpEsophagusResultTBody">
            {ntcps.map((item) => (
              <tr className="ntcpEsophagusResultTableCriteriaTR">
                <td
                  className="ntcpEsophagusResultTableCriteriaTD"
                  style={{
                    padding: '1px',
                    border: 'solid 1px grey',
                    color: item.color,
                    fontSize: '12px',
                    textAlign: 'left',
                  }}
                >
                  <div className="ntcpEsophagusResultTableCriteriaDiv">
                    <span
                      className="ntcpEsophagusResultTableCriteriaSpan"
                      style={{
                        textAlign: 'left',
                        fontSize: '13px',
                        padding: '1px',
                      }}
                    >
                      {item.criteria}
                    </span>
                  </div>
                </td>
                <td
                  className="ntcpEsophagusResultTableValueTD"
                  style={{
                    padding: '1px',
                    border: 'solid 1px grey',
                    color: item.color,
                    fontSize: '13px',
                    textAlign: 'center',
                  }}
                >
                  {item.value}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
    return result;
  } else {
    return (
      <label
        className="ntcpMessage"
        style={{
          color: 'red',
          textAlign: 'center',
          marginTop: '20px',
          width: '95%',
          fontSize: '18px',
        }}
      >
        Warning: No NTCP calculation available. Please check the matching channels to the available contours!
      </label>
    );
  }
}
