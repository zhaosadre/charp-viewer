import React, { Component } from 'react';
import { Icon } from '../../utils/icons/Icon';
import CustomClickMenu from './CustomClickMenu.js';
import ContourModification from './ContourModification.js';
import ContourModificationDialog from './ContourModificationDialog.js';
import { confirmAlert } from 'react-confirm-alert';
import { Dropdown, Button, Select } from 'antd';
import { AiOutlineDown } from 'react-icons/ai';
import 'react-confirm-alert/src/react-confirm-alert.css';
import './styles.css';

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function componentToHex(c) {
  var hex = c.toString(16);
  return hex.length == 1 ? '0' + hex : hex;
}

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function rgbToHex(r, g, b) {
  r = Math.round(r);
  g = Math.round(g);
  b = Math.round(b);
  return '#' + componentToHex(r) + componentToHex(g) + componentToHex(b);
}

//https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
function hexToRgb(hex) {
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)] : null;
}
class SegmentItem extends React.Component {
  constructor(props) {
    super(props);

    let modifying = this.props.seg == this.props.activeSegment;

    this.state = {
      activeSegment: this.props.activeSegment,
      seg: this.props.seg,
      visible: this.props.seg.getVisible() || modifying,
      showEdit: this.props.showEdit,
    };

    this.colorId = 'colorInput' + this.props.index;
    this.colorListener = (event) => this.colorEventListener(event);
  }

  toggleVisibility() {
    if (this.state.seg == this.props.activeSegment) return;

    this.state.seg.setVisible(!this.state.visible);
    this.setState({ visible: !this.state.visible });
  }

  toggleModification() {
    switch (this.props.activeSegment) {
      case null:
        this.props.onModificationStart(this.state.seg);
        break;
      case this.state.seg:
        this.props.onModificationEnd(this.state.seg);
        break;
      default:
        confirmAlert({
          title: 'Contour Modification',
          message: 'Please save current contour before proceeding to a new contour edition.',
          closeOnEscape: false,
          closeOnClickOutside: false,
          buttons: [
            {
              label: 'OK',
              onClick: () => {},
            },
          ],
        });
    }
  }

  componentDidMount() {
    const color = this.state.seg.getColor();
    let colorValue = rgbToHex(color[0], color[1], color[2]);

    const input = document.getElementById(this.colorId);

    input.addEventListener('input', this.colorListener);

    input.value = colorValue;

    this.componentDidUpdate();
  }

  colorEventListener(event) {
    const input = document.getElementById(this.colorId);
    this.state.seg.setColor(hexToRgb(input.value));
  }

  componentDidUpdate() {
    let modifying = this.props.seg == this.props.activeSegment;

    if (modifying && !this.state.visible) {
      this.props.seg.setVisible(true);
      this.setState({ visible: true });
    }
  }

  handleNavigate() {
    const position = this.state.seg.getCenter();

    if (position) this.props.setUserPosition(position);
  }

  render() {
    let modifying = this.props.seg == this.props.activeSegment;

    const segmentNumber = this.state.seg.getNumber();

    const tdStyle = {
      width: '18px',
      height: '18px',
      border: 'none',
      cursor: 'pointer',
      border: 'none',
      outline: 'none',
      padding: 0,
    };

    const iconName = this.state.visible ? 'eye' : 'eye-closed';
    const secondIconName = modifying ? 'save' : 'edit';

    // const itemContent = [this.renderRenameContourDrowDown(), 'Navigate to segment', 'Cancel'];

    const itemContent = ['Navigate to segment', 'Cancel'];

    const onItemClick = [() => {}, () => this.handleNavigate(), () => {}];

    const showEdit = this.state.showEdit ? (
      <td>
        <Icon name={secondIconName} onClick={() => this.toggleModification()} className="segmentVisibility" />
      </td>
    ) : null;
    const targetId = 'segmentTr' + segmentNumber;

    return (
      <React.Fragment>
        <CustomClickMenu isEnabled={() => true} targetId={targetId} itemContent={itemContent} onItemClick={onItemClick} />
        <tr className={modifying ? 'segment' + ' active' : 'segment'}>
          <td>
            <Icon name={iconName} onClick={() => this.toggleVisibility()} className="segmentVisibility" />
          </td>
          {showEdit}
          <td>
            <input id={this.colorId} style={tdStyle} type="color" />
          </td>
          <td id={targetId} style={{ cursor: 'pointer' }}>
            {this.state.seg.getLabel()}
          </td>
        </tr>
      </React.Fragment>
    );
  }
}

export default class ContourBrowser extends React.Component {
  constructor(props) {
    super(props);

    if (!this.props.segDataSet) {
      this.state = { segs: null };
      return;
    }

    this.state = {
      activeSegment: null,
      contourModificationHistoryDataSet: this.props.contourModificationHistoryDataSet,
      allVisible: false,
      showEdit: this.props.showEdit,
      selectedContour: null,
    };

    this.modificationEndListener = () => {
      const segs = this.props.segDataSet.getSegs();
      segs.forEach((seg) => {
        seg.setActive(false);
      });
      this.setState({ activeSegment: null });
    };

    this.contourModification = ContourModification.getInstance();
    this.contourModification.onModicationEnd(this.modificationEndListener);
    this.contourModificationFunction = {};
    this.updateItemsKey = 0;
  }

  componentWillUnmount() {
    this.contourModification.removeModificationEndListener(this.modificationEndListener);

    const input = document.getElementById(this.colorId);
    if (input) input.removeEventListener('input', this.colorListener);
  }

  componentDidMount() {
    this.props.segDataSet.addListener(() => {
      this.onSegChange();
    });
  }

  onSegChange() {
    this.setState({ segs: this.props.segDataSet.getSegs() });
  }

  onModificationStart(activeSegment) {
    const segs = this.props.segDataSet.getSegs();

    segs.forEach((seg) => {
      seg == activeSegment ? seg.setActive(true) : seg.setActive(false);
    });

    this.contourModification.setActiveSeg(activeSegment);

    this.setState({
      activeSegment,
    });
  }

  onModificationEnd(activeSegment) {
    const segs = this.props.segDataSet.getSegs();
    this.setState({
      selectedContour: segs.filter((seg) => seg == activeSegment)[0],
    });
    this.contourModificationFunction.setVisible(true);
  }

  finishModification(modification) {
    const segs = this.props.segDataSet.getSegs();
    const modifiedSegment = segs.filter((seg) => seg == this.state.activeSegment)[0];
    this.props.onAddHistory(modification, modifiedSegment).then(() => {
      this.contourModification.setActiveSeg(null);
      segs.forEach((seg) => {
        seg.setActive(false);
      });

      this.setState({
        activeSegment: null,
      });
    });
  }

  toggleAll() {
    const segs = this.props.segDataSet.getSegs();
    segs.forEach((seg) => {
      if (seg.getVisible() == this.state.allVisible) {
        // the active segment stays always visible
        seg.setVisible(seg == this.state.activeSegment || !this.state.allVisible);
      }
    });
    // force update of all SegmentItem's
    this.updateItemsKey++;
    this.setState({ allVisible: !this.state.allVisible });
  }

  render() {
    if (!this.props.segDataSet) return <div>No contour available</div>;

    const segs = this.props.segDataSet.getSegs();

    const SEGItems = segs.map((seg, i) => {
      return (
        <SegmentItem
          onModificationStart={(activeSegment) => this.onModificationStart(activeSegment)}
          onModificationEnd={(activeSegment) => this.onModificationEnd(activeSegment)}
          seg={seg}
          activeSegment={this.state.activeSegment}
          setUserPosition={(userPosition) => this.props.setUserPosition(userPosition)}
          showEdit={this.state.showEdit}
          index={i}
          key={this.updateItemsKey + '_' + i + '_' + this.props.segDataSet.SegSOPUID}
        />
      );
    });

    return (
      <div>
        <ContourModificationDialog
          className="contourModificationDialog"
          contourModificationFunction={this.contourModificationFunction}
          selectedContour={this.state.selectedContour}
          onDialogOK={(modification) => {
            this.finishModification(modification);
          }}
        ></ContourModificationDialog>
        <div className="segTableDiv">
          <table className="segTable">
            <thead>
              <tr className="segment">
                <td>
                  <Icon name={this.state.allVisible ? 'eye' : 'eye-closed'} onClick={() => this.toggleAll()} className="segmentVisibility" />
                </td>
                <td />
                <td />
                <td className="showHidenAllText">Show/Hide all</td>
              </tr>
            </thead>
            <tbody className={this.props.isPlanEvaluation ? 'segTableTBodyPlanEvaluation' : 'segTableTBody'}>{SEGItems}</tbody>
          </table>
        </div>
      </div>
    );
  }
}
