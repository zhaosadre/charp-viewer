import vtkImageData from '@kitware/vtk.js/Common/DataModel/ImageData';
import vtkDataArray from '@kitware/vtk.js/Common/Core/DataArray';
import vtkMath from '@kitware/vtk.js/Common/Core/Math';
import DicomDataLoader from './DicomDataLoader';
import { GaussianBlur } from '../utils/GaussianBlurFilter';

function arrayMax(a) {
  if (!a) return 0;
  let max = a[0];
  for (let i = 1; i < a.length; i++) {
    if (a[i] > max) max = a[i];
  }
  return max;
}

function isMultiple(x, d) {
  const r = Math.abs(x) / d;
  return Math.abs(Math.round(r) - r) < 1e-5;
}

export default class DoseDataSet {
  constructor() {
    this.dicomDataLoader = DicomDataLoader.getInstance();
    this.listeners = [];
    this.doses = [];
    this.listenersLastID = -1; //TODO change how listenerIDs are generated
  }

  async delete() {
    await this.dicomDataLoader.disable();
    await this.doses.forEach((item) => item.dose.delete());
    this.listeners = [];
    this.doses = [];
  }

  getDataLoader() {
    return this.dicomDataLoader;
  }

  setDataLoader(dataLoader) {
    this.dicomDataLoader = dataLoader;

    if (this.doses && this.doses.length) this.doses.forEach((dose) => dose.setDataLoader(dataLoader));
  }

  async loadDataAsync(StudyInstanceUID, DoseSeriesUID, DoseSOPUID) {
    const dose = new Dose();
    dose.setDataLoader(this.dicomDataLoader);
    await dose.loadData(StudyInstanceUID, DoseSeriesUID, DoseSOPUID);

    this.doses.push({
      StudyInstanceUID,
      DoseSeriesUID,
      DoseSOPUID,
      dose,
    });

    const event = {
      target: DoseSOPUID,
      type: 'created',
      value: null,
    };
    this.modified(event);
  }

  addListener(listener) {
    this.listeners.push(listener);
    this.listenersLastID = this.listenersLastID + 1;
    return this.listenersLastID;
  }

  modified(event) {
    this.listeners.slice().forEach((listener) => listener(event)); // Very important to slice because this.listeners could be changed by one of the listener running in forEach
  }

  getDoses() {
    return this.doses.map((dose) => dose.dose);
  }

  getDose(DoseSOPUID) {
    const filteredImage = this.doses.filter((dose) => dose.DoseSOPUID == DoseSOPUID);

    if (!filteredImage.length) return null;

    return filteredImage[0].dose;
  }
}

/******************
 * Dose
 *****************/
export class Dose {
  constructor() {
    this.DoseSeriesUID = null;
    this.DoseSOPUID = null;
    this.StudyInstanceUID = null;
    this.dicomDataLoader = DicomDataLoader.getInstance();
    this.listeners = [];

    this.metaData = null;
    this.data = null;
    this.VTKData = null;
    this.trueDirectionDiffers = false;

    this.color = [255, 255, 0];

    this.dataModified = false;
    this.active = false;

    this.calculatedDVHs = new Map(); //  seg -> DVH result
    this.segmentListeners = new Map(); //  seg -> listener
  }

  async delete() {
    await this.dicomDataLoader.disable();
    this.listeners = [];

    for (const [segment, listener] of this.segmentListeners) {
      segment.removeListener(listener);
    }
    this.segmentListeners = null;
    this.calculatedDVHs = null;

    if (this.VTKData) {
      this.VTKData.delete();
    }
    if (this.scalarArray) {
      this.scalarArray.delete();
    }
  }

  getDataLoader() {
    return this.dicomDataLoader;
  }

  setDataLoader(dataLoader) {
    this.dicomDataLoader = dataLoader;
  }

  setDoseColor(color) {
    this.color = color;
  }

  setBeamNumber(metaData) {
    if (metaData.DoseSummationType == 'BEAM') {
      metaData['beam_number'] = metaData.ReferencedRTPlanSequence[0].ReferencedFractionGroupSequence[0].ReferencedBeamSequence[0].ReferencedBeamNumber;
    } else if (metaData.DoseSummationType == 'PRIOR_TARGET') {
      metaData['beam_number'] = 'PRIOR_TARGET';
    } else if (metaData.DoseSummationType == 'PRIOR_TARGET_OAR') {
      metaData['beam_number'] = 'PRIOR_TARGET_OAR';
    } else {
      metaData['beam_number'] = 'PLAN';
    }

    return metaData;
  }

  async loadData(StudyInstanceUID, DoseSeriesUID, DoseSOPUID) {
    if (!DoseSOPUID) {
      return;
    }

    this.StudyInstanceUID = StudyInstanceUID;
    this.DoseSeriesUID = DoseSeriesUID;
    this.DoseSOPUID = DoseSOPUID;

    const metaData = await this.dicomDataLoader.getRTDOSEData(this.StudyInstanceUID, this.DoseSeriesUID, this.DoseSOPUID);

    if (metaData.DoseSummationType == 'BEAM') {
      metaData['beam_number'] = metaData.ReferencedRTPlanSequence[0].ReferencedFractionGroupSequence[0].ReferencedBeamSequence[0].ReferencedBeamNumber;
    } else if (metaData.DoseSummationType == 'PRIOR_TARGET') {
      metaData['beam_number'] = 'PRIOR_TARGET';
    } else if (metaData.DoseSummationType == 'PRIOR_TARGET_OAR') {
      metaData['beam_number'] = 'PRIOR_TARGET_OAR';
    } else {
      metaData['beam_number'] = 'PLAN';
    }

    this.metaData = metaData;

    if (!metaData)
      // could happen if we abort data loading
      return;

    const rawData = this.getDoseImage(metaData);
    this.data = new Float32Array(rawData.length);
    for (let i = 0; i < rawData.length; i++) this.data[i] = rawData[i] * metaData.DoseGridScaling;

    // destroy the raw data loaded from the dicom file to save memory
    metaData.PixelData = null;

    // testdata
    /*
    console.log("value ",this.data[100])
    for(let i=0;i<this.data.length;i++) {
      this.data[i]=i/100.0
    }
    console.log("value2 ",this.data[100])
    */

    // convert one-dimensional data to 3d grid data
    /*
    let gridData = []
    let elementOffset = 0
    for(let f=0;f<metaData.NumberOfFrames;f++) {
      let rows = []
      for(let r=0;r<metaData.Rows;r++) {
        rows.push(this.data.slice(elementOffset,elementOffset+metaData.Columns))
        elementOffset += metaData.Columns
      }
      gridData.push(rows)
    }
    */

    this.direction = metaData.ImageOrientationPatient;
    const columnStepToPatient = this.direction.slice(0, 3);
    const rowStepToPatient = this.direction.slice(3, 6);
    const direction3 = [0, 0, 0];
    vtkMath.cross(columnStepToPatient, rowStepToPatient, direction3);

    //console.log("GridFrameOffsetVector = ", metaData.GridFrameOffsetVector)

    //Note: If SliceThickness is None: SliceThickness = dcm.GridFrameOffsetVector[1]-dcm.GridFrameOffsetVector[0]
    if (!metaData.SliceThickness && metaData.GridFrameOffsetVector) {
      metaData.SliceThickness = Math.abs(metaData.GridFrameOffsetVector[1] - metaData.GridFrameOffsetVector[0]);
    }

    if (metaData.GridFrameOffsetVector && metaData.GridFrameOffsetVector[1] - metaData.GridFrameOffsetVector[0] < 0) {
      // reverse order of frames
      metaData.ImagePositionPatient[2] -= (metaData.NumberOfFrames - 1) * metaData.SliceThickness;
      const frameSize = metaData.Columns * metaData.Rows;
      const reversedFrames = new Float32Array(this.data.length);
      for (let f = 0; f < metaData.NumberOfFrames; f++) {
        for (let i = 0; i < frameSize; i++) {
          reversedFrames[(metaData.NumberOfFrames - f - 1) * frameSize + i] = this.data[f * frameSize + i];
        }
      }
      this.data = reversedFrames;
    }

    vtkMath.normalize(columnStepToPatient);
    vtkMath.normalize(rowStepToPatient);
    vtkMath.normalize(direction3);
    this.trueDirection = columnStepToPatient.concat(rowStepToPatient).concat(direction3);

    this.direction = this.trueDirection;
    const dir0 = this.direction.slice(0, 3);
    const dir1 = this.direction.slice(3, 6);
    const dir2 = this.direction.slice(6, 9);

    const dirs = [dir0, dir1, dir2];
    console.log('dir = ', dirs);
    const indM0 = dir0
      .map((e) => Math.abs(e))
      .indexOf(
        Math.max.apply(
          null,
          dir0.map((e) => Math.abs(e))
        )
      );
    const indM1 = dir1
      .map((e) => Math.abs(e))
      .indexOf(
        Math.max.apply(
          null,
          dir1.map((e) => Math.abs(e))
        )
      );
    const indM2 = dir2
      .map((e) => Math.abs(e))
      .indexOf(
        Math.max.apply(
          null,
          dir2.map((e) => Math.abs(e))
        )
      );
    if (indM0 != 0 && indM1 != 1 && indM2 != 2) {
      this.direction = [1, 0, 0, 0, 1, 0, 0, 0, -1];
      this.trueDirectionDiffers = true;
    }

    // we use the original data, but maybe
    // later it will be replaced by interpolated data
    this.interpolatedDose = this;

    this.modified();
  }

  static createVTKData(dose, scaleFactor) {
    var scaledValues;
    if (scaleFactor == 1.0) {
      scaledValues = dose.data;
    } else {
      scaledValues = new Float32Array(dose.data.length);
      for (let i = 0; i < dose.data.length; i++) scaledValues[i] = dose.data[i] * scaleFactor;
    }

    const vtkdata = vtkImageData.newInstance();
    vtkdata.setDimensions(dose.metaData.Columns, dose.metaData.Rows, dose.metaData.NumberOfFrames);
    vtkdata.setSpacing(dose.metaData.PixelSpacing[0], dose.metaData.PixelSpacing[1], dose.metaData.SliceThickness);
    vtkdata.setDirection(dose.direction);
    vtkdata.setOrigin(dose.metaData.ImagePositionPatient[0], dose.metaData.ImagePositionPatient[1], dose.metaData.ImagePositionPatient[2]);
    vtkdata.getPointData().setScalars(
      vtkDataArray.newInstance({
        name: 'Scalars',
        numberOfComponents: 1,
        values: scaledValues,
      })
    );
    return vtkdata;
  }

  /**
   * returns VTK data for the difference between this dose and another dose,
   * regridded to the grid of the image
   */
  getDiffDoseVTKData(otherDose, maximumDose, thisScaleFactor, otherScaleFactor, image) {
    // Always assumes that ImagePatientOrientation is (1,0,0),(0,1,0)

    const imageMetaData = image.metaData[0];
    const imageSlices = image.metaData.length;

    const dx = imageMetaData.PixelSpacing[0];
    const dy = imageMetaData.PixelSpacing[1];
    const dz = imageMetaData.SliceThickness;

    const regriddedDose1 = this.getRegriddedDose(
      [
        imageMetaData.ImagePositionPatient[0],
        imageMetaData.ImagePositionPatient[1],
        imageMetaData.ImagePositionPatient[2] - imageSlices * imageMetaData.SliceThickness,
      ],
      dx,
      dy,
      dz
    );

    const regriddedDose2 = otherDose.getRegriddedDose(
      [
        imageMetaData.ImagePositionPatient[0],
        imageMetaData.ImagePositionPatient[1],
        imageMetaData.ImagePositionPatient[2] - imageSlices * imageMetaData.SliceThickness,
      ],
      dx,
      dy,
      dz
    );

    const metaData1 = regriddedDose1.metaData;
    const metaData2 = regriddedDose2.metaData;
    const originX1 = metaData1.ImagePositionPatient[0];
    const originY1 = metaData1.ImagePositionPatient[1];
    const originZ1 = metaData1.ImagePositionPatient[2];
    const originX2 = metaData2.ImagePositionPatient[0];
    const originY2 = metaData2.ImagePositionPatient[1];
    const originZ2 = metaData2.ImagePositionPatient[2];
    const doseNumberOfFrames1 = metaData1.NumberOfFrames;
    const doseRows1 = metaData1.Rows;
    const doseColumns1 = metaData1.Columns;
    const doseFrameSize1 = doseColumns1 * doseRows1;
    const doseNumberOfFrames2 = metaData2.NumberOfFrames;
    const doseRows2 = metaData2.Rows;
    const doseColumns2 = metaData2.Columns;
    const doseFrameSize2 = doseColumns2 * doseRows2;

    const diffOriginX = Math.min(originX1, originX2);
    const diffOriginY = Math.min(originY1, originY2);
    const diffOriginZ = Math.min(originZ1, originZ2);
    const diffMaxX = Math.max((metaData1.Columns - 1) * dx + originX1, (metaData2.Columns - 1) * dx + originX2);
    const diffMaxY = Math.max((metaData1.Rows - 1) * dy + originY1, (metaData2.Rows - 1) * dy + originY2);
    const diffMaxZ = Math.max((metaData1.NumberOfFrames - 1) * dz + originZ1, (metaData2.NumberOfFrames - 1) * dz + originZ2);
    const diffColumns = Math.round((diffMaxX - diffOriginX) / dx + 1);
    const diffRows = Math.round((diffMaxY - diffOriginY) / dy + 1);
    const diffSlices = Math.round((diffMaxZ - diffOriginZ) / dz + 1);
    const diffFrameSize = diffColumns * diffRows;

    const diffDoseData = new Float32Array(diffColumns * diffRows * diffSlices);

    for (var slice = 0; slice < diffSlices; slice++) {
      for (var row = 0; row < diffRows; row++) {
        for (var col = 0; col < diffColumns; col++) {
          // calculate (x,y,z) position of this point
          const x = col * dx + diffOriginX;
          const y = row * dy + diffOriginY;
          const z = slice * dz + diffOriginZ;
          // find dose value of dose 1
          const doseCol1 = Math.round((x - originX1) / dx);
          const doseRow1 = Math.round((y - originY1) / dy);
          const doseSlice1 = Math.round((z - originZ1) / dz);
          const doseValue1 =
            doseCol1 >= 0 && doseCol1 < doseColumns1 && doseRow1 >= 0 && doseRow1 < doseRows1 && doseSlice1 >= 0 && doseSlice1 < doseNumberOfFrames1
              ? regriddedDose1.data[doseCol1 + doseRow1 * doseColumns1 + doseSlice1 * doseFrameSize1]
              : 0;
          // find dose value of dose 2
          const doseCol2 = Math.round((x - originX2) / dx);
          const doseRow2 = Math.round((y - originY2) / dy);
          const doseSlice2 = Math.round((z - originZ2) / dz);
          const doseValue2 =
            doseCol2 >= 0 && doseCol2 < doseColumns2 && doseRow2 >= 0 && doseRow2 < doseRows2 && doseSlice2 >= 0 && doseSlice2 < doseNumberOfFrames2
              ? regriddedDose2.data[doseCol2 + doseRow2 * doseColumns2 + doseSlice2 * doseFrameSize2]
              : 0;

          const diffDoseValue = (doseValue1 * thisScaleFactor - doseValue2 * otherScaleFactor) / maximumDose;
          diffDoseData[col + row * diffColumns + slice * diffFrameSize] = diffDoseValue;
        }
      }
    }

    const diffDose = {
      metaData: {
        ImagePositionPatient: [diffOriginX, diffOriginY, diffOriginZ],
        Columns: diffColumns,
        Rows: diffRows,
        NumberOfFrames: diffSlices,
        PixelSpacing: [dx, dy],
        SliceThickness: dz,
      },
      data: diffDoseData,
      direction: this.direction,
    };
    return Dose.createVTKData(diffDose, 1.0);
  }

  // Calculates and returns DVH for a segment.
  // Caches results.
  // Throws Error if problem in data.
  getDVH(seg, numberOfBins) {
    // Have we already calculated the DVH for this dose and this segment?
    if (this.calculatedDVHs.has(seg)) {
      return this.calculatedDVHs.get(seg);
    }

    // Check whether the orientation of the segment data and the
    // dose data are compatible.
    if (
      seg.metaData.ImageOrientationPatient.length != this.metaData.ImageOrientationPatient.length ||
      !seg.metaData.ImageOrientationPatient.every(function (e, i) {
        return e === this.metaData.ImageOrientationPatient[i];
      }, this)
    ) {
      throw new Error('Orientation of segment data and dose data are not identical');
    }

    // get dose regridded to the same grid as the segment.
    // note that Z direction of segment is inverted.
    const regriddedDose = this.getRegriddedDose(
      [
        seg.metaData.ImagePositionPatient[0],
        seg.metaData.ImagePositionPatient[1],
        seg.metaData.ImagePositionPatient[2] - seg.metaData.Slices * seg.metaData.SliceThickness,
      ],
      seg.metaData.PixelSpacing[0],
      seg.metaData.PixelSpacing[1],
      seg.metaData.SliceThickness
    );

    // get the dose values for the voxels in the segment
    const doseValues = Dose.getDoseInSegment(regriddedDose, seg);

    // calculate mean of dose values
    const mean = doseValues.reduce((a, b) => a + b, 0) / doseValues.length;

    // calculate histogram of dose values
    const doseMax = arrayMax(doseValues);
    const binSize = doseMax / numberOfBins;
    const histogram = new Int32Array(numberOfBins);
    doseValues.forEach((v) => histogram[Math.trunc(v / binSize)]++);

    // calculate cummulative DVH
    const dvh = new Int32Array(numberOfBins + 1);
    dvh[0] = doseValues.length;
    for (let i = 1; i < dvh.length; i++) {
      dvh[i] = dvh[i - 1] - histogram[i - 1];
    }

    const result = {
      seg: seg,
      mean: mean,
      max: doseMax,
      x: [...Array(numberOfBins + 1).keys()].map((e) => e * binSize), // generate bin values
      y: dvh.map((e) => (e * 100.0) / doseValues.length), // scale DVH to 100%
    };

    // Store the result, so we don't have to calculate it again
    this.calculatedDVHs.set(seg, result);
    // Add listener to delete the calculated DVH if segment data changes
    this.segmentListeners.set(seg, (whatChange) => {
      if (whatChange === 'data') {
        this.calculatedDVHs.delete(seg);
        seg.removeListener(this.segmentListeners.get(seg));
        this.segmentListeners.delete(seg);
      }
    });
    seg.addListener(this.segmentListeners.get(seg));

    return result;
  }

  getRegriddedDose(gridOrigin, gridDx, gridDy, gridDz) {
    // Always assumes that ImagePatientOrientation is (1,0,0),(0,1,0)

    // Check if we need to interpolate
    const metaData = this.interpolatedDose.metaData;
    if (
      metaData.PixelSpacing[0] == gridDx &&
      metaData.PixelSpacing[1] == gridDy &&
      metaData.SliceThickness == gridDz &&
      isMultiple(metaData.ImagePositionPatient[0] - gridOrigin[0], gridDx) &&
      isMultiple(metaData.ImagePositionPatient[1] - gridOrigin[1], gridDy) &&
      isMultiple(metaData.ImagePositionPatient[2] - gridOrigin[2], gridDz)
    ) {
      return this.interpolatedDose;
    } else {
      this.interpolatedDose = Dose.interpolateDose(this, gridOrigin, gridDx, gridDy, gridDz);
      return this.interpolatedDose;
    }
  }

  isInterpolated() {
    return this.interpolatedDose != this;
  }

  static interpolateDose(dose, gridOrigin, gridDx, gridDy, gridDz) {
    // Always assumes that ImagePatienOrientation is (1,0,0),(0,1,0)
    //  x = column
    //  y = row
    //  z = slice

    const doseMetaData = dose.metaData;
    const doseRows = doseMetaData.Rows;
    const doseColumns = doseMetaData.Columns;
    const doseSlices = doseMetaData.NumberOfFrames;
    const doseFrameSize = doseColumns * doseRows;
    const doseDx = doseMetaData.PixelSpacing[0];
    const doseDy = doseMetaData.PixelSpacing[1];
    const doseDz = doseMetaData.SliceThickness;
    const doseOriginX = doseMetaData.ImagePositionPatient[0];
    const doseOriginY = doseMetaData.ImagePositionPatient[1];
    const doseOriginZ = doseMetaData.ImagePositionPatient[2];

    // anti-aliasing filtering
    const sigma = [0, 0, 0];
    if (gridDx > doseDx) sigma[0] = 0.4 * (gridDx / doseDx);
    if (gridDy > doseDy) sigma[1] = 0.4 * (gridDy / doseDy);
    if (gridDz > doseDz) sigma[2] = 0.4 * (gridDz / doseDz);
    const doseData = sigma[0] != 0 || sigma[1] != 0 || sigma[2] != 0 ? GaussianBlur(dose.data, doseRows, doseColumns, doseSlices, sigma) : dose.data;

    // find a new dose origin that fits into the image grid
    // but that is still inside the dose data
    const newDoseOriginX = Math.ceil((doseOriginX - gridOrigin[0]) / gridDx) * gridDx + gridOrigin[0];
    const newDoseOriginY = Math.ceil((doseOriginY - gridOrigin[1]) / gridDy) * gridDy + gridOrigin[1];
    const newDoseOriginZ = Math.ceil((doseOriginZ - gridOrigin[2]) / gridDz) * gridDz + gridOrigin[2];

    // calculate new size of dose.
    // the new dose cube should be inside the old dose data
    const maxX = (doseColumns - 1) * doseDx + doseOriginX;
    const maxY = (doseRows - 1) * doseDy + doseOriginY;
    const maxZ = (doseSlices - 1) * doseDz + doseOriginZ;
    const newMaxX = Math.floor((maxX - gridOrigin[0]) / gridDx) * gridDx + gridOrigin[0];
    const newMaxY = Math.floor((maxY - gridOrigin[1]) / gridDy) * gridDy + gridOrigin[1];
    const newMaxZ = Math.floor((maxZ - gridOrigin[2]) / gridDz) * gridDz + gridOrigin[2];
    const newColumns = Math.round((newMaxX - newDoseOriginX) / gridDx + 1);
    const newRows = Math.round((newMaxY - newDoseOriginY) / gridDy + 1);
    const newSlices = Math.round((newMaxZ - newDoseOriginZ) / gridDz + 1);
    const newFrameSize = newColumns * newRows;

    const newDoseData = new Float32Array(newColumns * newRows * newSlices);

    for (var slice = 0; slice < newSlices; slice++) {
      for (var row = 0; row < newRows; row++) {
        for (var col = 0; col < newColumns; col++) {
          // calculate (x,y,z) position of this point
          const x = col * gridDx + newDoseOriginX;
          const y = row * gridDy + newDoseOriginY;
          const z = slice * gridDz + newDoseOriginZ;
          // find position in old dose data
          const doseCol = Math.floor((x - doseOriginX) / doseDx);
          const doseRow = Math.floor((y - doseOriginY) / doseDy);
          const doseSlice = Math.floor((z - doseOriginZ) / doseDz);
          const doseX = doseCol * doseDx + doseOriginX;
          const doseY = doseRow * doseDy + doseOriginY;
          const doseZ = doseSlice * doseDz + doseOriginZ;
          // trilinear interpolation
          // https://en.wikipedia.org/wiki/Trilinear_interpolation
          const xd = (x - doseX) / doseDx;
          const yd = (y - doseY) / doseDy;
          const zd = (z - doseZ) / doseDz;
          // the dose values in the corners of the interpolation cube
          const i = doseCol + doseRow * doseColumns + doseSlice * doseFrameSize;

          const c000 = doseData[i];
          const c100 = xd > 0 ? doseData[i + 1] : 0;
          const c010 = yd > 0 ? doseData[i + doseColumns] : 0;
          const c110 = xd > 0 && yd > 0 ? doseData[i + 1 + doseColumns] : 0;

          const c001 = zd > 0 ? doseData[i] : 0;
          const c101 = xd > 0 && zd > 0 ? doseData[i + 1 + doseFrameSize] : 0;
          const c011 = yd > 0 && zd > 0 ? doseData[i + doseColumns + doseFrameSize] : 0;
          const c111 = xd > 0 && yd > 0 && zd > 0 ? doseData[i + 1 + doseColumns + doseFrameSize] : 0;

          const c00 = c000 * (1.0 - xd) + c100 * xd;
          const c01 = c001 * (1.0 - xd) + c101 * xd;
          const c10 = c010 * (1.0 - xd) + c110 * xd;
          const c11 = c011 * (1.0 - xd) + c111 * xd;

          const c0 = c00 * (1.0 - yd) + c10 * yd;
          const c1 = c01 * (1.0 - yd) + c11 * yd;

          const c = c0 * (1 - zd) + c1 * zd;

          newDoseData[col + row * newColumns + slice * newFrameSize] = c;
        }
      }
    }

    return {
      metaData: {
        ImagePositionPatient: [newDoseOriginX, newDoseOriginY, newDoseOriginZ],
        Columns: newColumns,
        Rows: newRows,
        NumberOfFrames: newSlices,
        PixelSpacing: [gridDx, gridDy],
        SliceThickness: gridDz,
      },
      data: newDoseData,
      direction: dose.direction,
    };
  }

  // returns a list that contains the dose values for the voxels of the segment
  static getDoseInSegment(dose, seg) {
    // Always assumes that ImagePatienOrientation is (1,0,0),(0,1,0)
    //  x = column
    //  y = row
    //  z = slice

    const segData = seg.getData();
    const segMetaData = seg.metaData;
    const segSlices = segMetaData.Slices;
    const segRows = segMetaData.Rows;
    const segColumns = segMetaData.Columns;
    const segSliceSize = segColumns * segRows;
    const segPixelSpacingX = segMetaData.PixelSpacing[0];
    const segPixelSpacingY = segMetaData.PixelSpacing[1];
    const segPixelSpacingZ = segMetaData.SliceThickness;
    const segImagePositionX = segMetaData.ImagePositionPatient[0];
    const segImagePositionY = segMetaData.ImagePositionPatient[1];
    const segImagePositionZ = segMetaData.ImagePositionPatient[2];

    const doseData = dose.data;
    const doseMetaData = dose.metaData;
    const doseNumberOfFrames = doseMetaData.NumberOfFrames;
    const doseRows = doseMetaData.Rows;
    const doseColumns = doseMetaData.Columns;
    const doseFrameSize = doseColumns * doseRows; // the size of  one slice
    const dosePixelSpacingX = doseMetaData.PixelSpacing[0];
    const dosePixelSpacingY = doseMetaData.PixelSpacing[1];
    const dosePixelSpacingZ = doseMetaData.SliceThickness;
    const doseImagePositionX = doseMetaData.ImagePositionPatient[0];
    const doseImagePositionY = doseMetaData.ImagePositionPatient[1];
    const doseImagePositionZ = doseMetaData.ImagePositionPatient[2];

    // const diffX = (segImagePositionX - doseImagePositionX) / dosePixelSpacingX
    // const diffY = (segImagePositionY - doseImagePositionY) / dosePixelSpacingY
    // const diffZ = (segImagePositionZ - doseImagePositionZ) / dosePixelSpacingZ
    // const fx = segPixelSpacingX / dosePixelSpacingX
    // const fy = segPixelSpacingY / dosePixelSpacingY
    // const fz = -segPixelSpacingZ / dosePixelSpacingZ

    // go through all elements in the segment data array
    const doseValues = [];
    loopAll: for (let slice = 0; slice < segSlices; slice++) {
      for (let row = 0; row < segRows; row++) {
        for (let col = 0; col < segColumns; col++) {
          var posInArray = col + row * segColumns + slice * segSliceSize;
          const segValue = segData[posInArray];

          // segValue==0 means that the element is not inside the segment ROI
          // and should be ignored for the dose calculation
          if (segValue === 0) {
            // go quickly through region that does not belong to the segment
            posInArray = segData.indexOf(1, posInArray + 1);
            if (posInArray === -1) {
              break loopAll;
            }
            slice = Math.floor(posInArray / segSliceSize);
            posInArray -= slice * segSliceSize;
            row = Math.floor(posInArray / segColumns);
            col = posInArray - row * segColumns;
          }

          // calculate the (x,y,z) position of the element in the segment data array
          const x = col * segPixelSpacingX + segImagePositionX;
          const y = row * segPixelSpacingY + segImagePositionY;
          const z = -slice * segPixelSpacingZ + segImagePositionZ;

          // calculate the index of the position in the dose data
          const doseCol = Math.round((x - doseImagePositionX) / dosePixelSpacingX);
          const doseRow = Math.round((y - doseImagePositionY) / dosePixelSpacingY);
          const doseSlice = Math.round((z - doseImagePositionZ) / dosePixelSpacingZ);

          /*
          const doseCol = Math.round( col*fx + diffX )
          const doseRow = Math.round( row*fy + diffY )
          const doseSlice = Math.round( slice*fz + diffZ )
          */

          // is the point inside the dose data array?
          if (doseCol >= 0 && doseCol < doseColumns && doseRow >= 0 && doseRow < doseRows && doseSlice >= 0 && doseSlice < doseNumberOfFrames) {
            doseValues.push(doseData[doseCol + doseRow * doseColumns + doseSlice * doseFrameSize]);
          } else {
            // no dose information for this position
            doseValues.push(0);
          }
        }
      }
    }

    return doseValues;
  }

  addListener(listener) {
    const date = new Date();
    const id = date.getTime() + Math.round(Math.random() * 100);

    const listenerObj = {
      listener,
      listenerId: id,
    };
    this.listeners.push(listenerObj);
    return id;
  }

  removeListener(listener) {
    const listenerIndex = this.listeners.indexOf(listener);

    if (listenerIndex > -1) this.listeners.splice(listenerIndex, 1);
  }

  modified(what) {
    if (what == 'data') {
      this.dataModified = true;
    }

    this.listeners.slice().forEach((listenerObj) => {
      listenerObj.listener(what);
    }); // Very important to slice because this.listeners could be changed by one of the listener running in forEach
  }

  getLabel() {
    return this.metaData.SeriesDescription;
  }

  getMetaData() {
    return this.metaData;
  }

  getColor() {
    return this.color;
  }

  setColor(color) {
    if (color == this.color) return;

    this.color = color.slice();
    this.modified('color');
  }

  getDoseSeriesUID() {
    return this.DoseSeriesUID;
  }

  getDoseSOPUID() {
    return this.DoseSOPUID;
  }

  getStudyInstanceUID() {
    return this.StudyInstanceUID;
  }

  getData() {
    return this.data;
  }

  /**
   * returns VTK data for the dose regridded to the grid of the image
   */
  getVTKData(scaleFactor, image) {
    const imageMetaData = image.metaData[0];
    const imageSlices = image.metaData.length;

    const regriddedDose = this.getRegriddedDose(
      [
        imageMetaData.ImagePositionPatient[0],
        imageMetaData.ImagePositionPatient[1],
        imageMetaData.ImagePositionPatient[2] - imageSlices * imageMetaData.SliceThickness,
      ],
      imageMetaData.PixelSpacing[0],
      imageMetaData.PixelSpacing[1],
      imageMetaData.SliceThickness
    );

    this.VTKData = Dose.createVTKData(regriddedDose, scaleFactor);
    return this.VTKData;
  }

  euclidean_dist = function (a, b) {
    return (
      a
        .map((x, i) => Math.abs(x - b[i]) ** 2) // square the difference
        .reduce((sum, now) => sum + now) ** // sum
      (1 / 2)
    );
  };

  getDoseImage = function (metaData) {
    const numBytes = metaData.PixelData[0].byteLength;
    const numBytesPerValue = metaData.BitsStored / 8;
    const numValues = numBytes / numBytesPerValue;
    const isSigned = metaData.PixelRepresentation == 1;
    const isLittleEndian = metaData.HighBit == metaData.BitsStored - 1;
    const dataview = new DataView(metaData.PixelData[0]);

    let dt = null;
    if (numBytesPerValue == 2 && !isSigned) {
      dt = new Uint16Array(numValues);
      for (var i = 0; i < numValues; i++) dt[i] = dataview.getUint16(i * numBytesPerValue, isLittleEndian);
    } else if (numBytesPerValue == 2 && isSigned) {
      dt = new Int16Array(numValues);
      for (var i = 0; i < numValues; i++) dt[i] = dataview.getInt16(i * numBytesPerValue, isLittleEndian);
    } else if (numBytesPerValue == 4 && !isSigned) {
      dt = new Uint32Array(numValues);
      for (var i = 0; i < numValues; i++) dt[i] = dataview.getUint32(i * numBytesPerValue, isLittleEndian);
    } else if (numBytesPerValue == 4 && isSigned) {
      dt = new Int32Array(numValues);
      for (var i = 0; i < numValues; i++) dt[i] = dataview.getInt32(i * numBytesPerValue, isLittleEndian);
    } else {
      console.log('Error: Unkown data type for ' + metaData);
      return new Uint16Array();
    }
    return dt;
  };

  sendData() {
    // TODO
  }
}
