import DicomDataLoader from './DicomDataLoader';

const ORTHANC_ATTACHMENT_ID = 42000;

export default class ContourModificationHistoryDataSet {
  constructor() {
    this.DicomDataLoader = DicomDataLoader.getInstance();
    this.listeners = [];
    this.histories = [];
    this.listenersLastID = -1;
    this.cachedOrthancInstanceID = null;
    this.cachedStudyInstanceUID = null;
  }

  async delete() {
    if (this.dicomDataLoader) {
      await this.dicomDataLoader.disable();
    }
    if (this.histories) {
      await this.histories.forEach((item) => item.delete());
    }
    this.listeners = [];
    this.histories = [];
    this.cachedOrthancInstanceID = null;
    this.cachedStudyInstanceUID = null;
  }

  getDataLoader() {
    return this.dicomDataLoader;
  }

  setDataLoader(dataLoader) {
    this.dicomDataLoader = dataLoader;
    if (this.histories && this.histories.length) {
      this.histories.forEach((history) => history.setDataLoader(dataLoader));
    }
  }

  // returns the Orthanc instance ID of the study
  // returns null if study not found
  async getOrthancInstanceID(StudyInstanceUID) {
    if (this.cachedStudyInstanceUID == StudyInstanceUID && this.cachedOrthancInstanceID != null) {
      return this.cachedOrthancInstanceID;
    }

    const listOfStudyIDs = await (await fetch('/getstudy', { method: 'GET' })).json();
    for (var i = 0; i < listOfStudyIDs.length; i++) {
      const studyInfo = await (await fetch('/getstudy/' + listOfStudyIDs[i], { method: 'GET' })).json();
      if (studyInfo.MainDicomTags.StudyInstanceUID == StudyInstanceUID) {
        this.cachedStudyInstanceUID = StudyInstanceUID;
        this.cachedOrthancInstanceID = listOfStudyIDs[i];
        return this.cachedOrthancInstanceID;
      }
    }
    return null;
  }

  async loadDataAsync(StudyInstanceUID) {
    const iid = await this.getOrthancInstanceID(StudyInstanceUID);
    if (iid == null) return;

    const result = await fetch('/modificationhistory/' + iid + '/attachments/' + ORTHANC_ATTACHMENT_ID + '/data', { method: 'GET' });
    if (result.status != 200) {
      return;
    }
    (await result.json()).forEach((item) => {
      this.histories.push(new History(item));
    }, this);

    const event = {
      target: 'historyEvent',
      type: 'created',
      value: null,
    };
    this.modified(event);
  }

  /**
   * Save complete history in attachment ORTHANC_ATTACHMENT_ID
   */
  async saveInfoData(iid) {
    const allInfoData = JSON.stringify(this.histories.map((history) => history.getModificationInfoData()));
    await fetch('/modificationhistory/' + iid + '/attachments/' + ORTHANC_ATTACHMENT_ID, { method: 'PUT', body: allInfoData });
  }

  /**
   * Save modified segment data in attachment
   */
  async saveModifiedSegmentData(iid, modifiedSegData, originalSegData, attachmentID) {
    // do delta + runlength compression
    const dataLength = modifiedSegData.length;
    var compressedData = [];
    var previousSymbol = -100;
    var count = 0;
    for (var i = 0; i < dataLength; i++) {
      var symbol = modifiedSegData[i] - originalSegData[i];
      if (symbol == previousSymbol) {
        count++;
      } else {
        if (count != 0) {
          compressedData.push(previousSymbol, count);
        }
        previousSymbol = symbol;
        count = 1;
      }
    }
    compressedData.push(previousSymbol, count);

    // save on server
    await fetch('/modificationhistory/' + iid + '/attachments/' + attachmentID, { method: 'PUT', body: JSON.stringify(compressedData) });
  }

  /**
   * Adds a new modification for a segment to the history.
   * If successful, the modificationInfoData will have an attachmentID field.
   */
  async addHistory(modificationInfoData, modifiedSegment, StudyInstanceUID) {
    const iid = await this.getOrthancInstanceID(StudyInstanceUID);
    if (iid == null) return;

    // list of all IDs currently in use
    const usedIDs = this.histories.map((history) => history.getModificationInfoData().attachmentID);
    // find a free attachment ID
    var freeID = -1;
    for (var id = ORTHANC_ATTACHMENT_ID + 1; id < ORTHANC_ATTACHMENT_ID + 5000; id++) {
      if (usedIDs.indexOf(id) == -1) {
        freeID = id;
        break;
      }
    }
    if (freeID == -1) {
      // no free ID found! We cannot add the new entry
      return;
    }

    // add the new entry to the history
    modificationInfoData.attachmentID = freeID;
    this.histories.push(new History(modificationInfoData));

    // save modified segment data
    const originalSegData = modifiedSegment.getServerData();
    const modifiedSegData = modifiedSegment.getData();
    await this.saveModifiedSegmentData(iid, modifiedSegData, originalSegData, freeID);
    // save complete history
    await this.saveInfoData(iid);
  }

  async removeHistory(modificationInfoData, StudyInstanceUID) {
    const iid = await this.getOrthancInstanceID(StudyInstanceUID);
    if (iid == null) return;

    // remove the entry from the history list
    this.histories = this.histories.filter((history) => history.getModificationInfoData().attachmentID != modificationInfoData.attachmentID);

    // save complete history
    await this.saveInfoData(iid);
    // delete modified segment data
    await fetch('/modificationhistory/' + iid + '/attachments/' + modificationInfoData.attachmentID, { method: 'DELETE' });
  }

  /**
   * Gets the specified version "attachmentID" of the data for a segment.
   * If attachmentID==-1, the latest version of the segment will be returned.
   * If attachmentID==null, the original version of the segment will be returned.
   */
  async getSegmentData(segment, attachmentID, StudyInstanceUID) {
    const segmentID = segment.getID();
    const serverData = segment.getServerData();
    const iid = await this.getOrthancInstanceID(StudyInstanceUID);
    if (!iid || attachmentID == null) {
      // study ID not found on server -> return the original server data
      return { data: serverData, attachmentID: null };
    }

    if (attachmentID == -1) {
      // find latest modification of the segment
      const allModifications = this.histories.filter((history) => history.getModificationInfoData().contourID == segmentID);
      if (allModifications.length == 0) {
        // there is no modification. just return the original server data
        return { data: serverData, attachmentID: null };
      } else {
        attachmentID = allModifications[allModifications.length - 1].getModificationInfoData().attachmentID;
      }
    }

    // get the data from the server
    const result = await fetch('/modificationhistory/' + iid + '/attachments/' + attachmentID + '/data', { method: 'GET' });
    if (result.status != 200) {
      // error -> return the original server data
      return { data: serverData, attachmentID: null };
    }

    // decompress the data and apply the difference to original server data
    const compressedData = await result.json();
    const data = new Uint8Array(serverData.length);
    var writePosition = 0;
    for (var i = 0; i < compressedData.length; i += 2) {
      const symbol = compressedData[i];
      const count = compressedData[i + 1];
      for (var j = 0; j < count; j++) {
        data[writePosition] = serverData[writePosition] + symbol;
        writePosition++;
      }
    }
    // data is ready
    return { data, attachmentID };
  }

  async getAllContourModifications(StudyInstanceUID) {
    const iid = await this.getOrthancInstanceID(StudyInstanceUID);
    const attachments = [];
    // load the history into the dataset
    await this.loadDataAsync(StudyInstanceUID);
    // load the modifications
    for (var i = 0; i < this.histories.length; i++) {
      // get the data for the modification from the server
      const attachmentID = this.histories[i].modificationInfoData.attachmentID;
      const result = await fetch('/modificationhistory/' + iid + '/attachments/' + attachmentID + '/data', { method: 'GET' });
      if (result.status == 200) {
        const data = await result.json();
        attachments.push({ modificationInfoData: this.histories[i].modificationInfoData, data: data });
      }
    }
    return { StudyInstanceUID: StudyInstanceUID, history: attachments };
  }

  /**
   * This function assumes that history is empty!
   */
  async setContourModifications(StudyInstanceUID, attachments) {
    const iid = await this.getOrthancInstanceID(StudyInstanceUID);

    // save data of all attachments
    for (var i = 0; i < attachments.length; i++) {
      const entry = attachments[i];
      this.histories.push(new History(entry.modificationInfoData));
      await fetch('/modificationhistory/' + iid + '/attachments/' + entry.modificationInfoData.attachmentID, {
        method: 'PUT',
        body: JSON.stringify(entry.data),
      });
    }

    // save complete history
    await this.saveInfoData(iid);
  }

  addListener(listener) {
    this.listeners.push(listener);
    this.listenersLastID = this.listenersLastID + 1;
    return this.listenersLastID;
  }

  modified(event) {
    this.listeners.slice().forEach((listener) => listener(event));
  }

  getHistoryInfoData() {
    return this.histories.map((history) => history.getModificationInfoData());
  }
}

/****************
 * History
 ****************/
class History {
  constructor(modificationInfoData) {
    this.modificationInfoData = modificationInfoData;
  }

  delete() {
    // do nothing. don't remove.
  }

  getModificationInfoData() {
    return this.modificationInfoData;
  }
}
