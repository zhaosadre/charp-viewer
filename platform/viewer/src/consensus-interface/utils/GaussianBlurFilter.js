
export function GaussianBlur(originalImage, width, height, depth, sigma) {
    const heightWidth = width*height

    var t, size, kernel, offset
    var image = originalImage.slice(0)
    var scratch = new Float32Array(originalImage.length)

    t = image; image=scratch; scratch=t

    // X
    size = getKernelSizeForSigma(sigma[0])
    kernel = new Float32Array(size)
    fillGaussianBlurKernel1D(sigma[0], size, kernel)
    offset = Math.floor(size/2)

    for (var z = 0; z < depth; z++) {
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                const idx = z*heightWidth + y*width + x;
                const new_offset_neg = x - offset >= 0 ? -offset :  -x;
                const new_offset_pos = x + offset < width ? offset : width - x - 1;
                var value = 0.0;
                var i;
                for (i = -offset; i < new_offset_neg; i++) {
                    value += scratch[idx + new_offset_neg] * kernel[i+offset];
                }
                for (; i <= new_offset_pos; i++) {
                    value += scratch[idx + i] * kernel[i+offset];
                }
                for (; i <= offset; i++) {
                    value += scratch[idx + new_offset_pos] * kernel[i+offset];
                }
                image[idx] = value
            }
        }
    }
    t = image; image=scratch; scratch=t

    // Y
    size = getKernelSizeForSigma(sigma[1])
    kernel = new Float32Array(size)
    fillGaussianBlurKernel1D(sigma[1], size, kernel)
    offset = Math.floor(size/2)

    for (var z = 0; z < depth; z++) {
        for (var y = 0; y < height; y++) {
            const new_offset_neg = y - offset >= 0 ? -offset :  -y;
            const new_offset_pos = y + offset < height ? offset : height - y - 1;
            for (var x = 0; x < width; x++) {
              const idx = z*heightWidth + y*width + x;
              var value = 0.0;
              var i;      
              for (i = -offset; i < new_offset_neg; i++) {
                value += scratch[idx + new_offset_neg*width] * kernel[i+offset];
              }
              for (; i <= new_offset_pos; i++) {
                value += scratch[idx + i*width] * kernel[i+offset];
              }
              for (; i <= offset; i++) {
                value += scratch[idx + new_offset_pos*width] * kernel[i+offset];
              }
              image[idx] = value
            }
        }
    }
    t = image; image=scratch; scratch=t

    // Z
    size = getKernelSizeForSigma(sigma[2])
    kernel = new Float32Array(size)
    fillGaussianBlurKernel1D(sigma[2], size, kernel)
    offset = Math.floor(size/2)

    for (var z = 0; z < depth; z++) {
        const new_offset_neg = z - offset >= 0 ? -offset :  -z;
        const new_offset_pos = z + offset < depth ? offset : depth - z - 1;
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                const idx = z*heightWidth + y*width + x;
                var value = 0.0;
                var i;
                for (i = -offset; i < new_offset_neg; i++) {
                    value += scratch[idx + new_offset_neg*heightWidth] * kernel[i+offset];
                }
                for (; i <= new_offset_pos; i++) {
                    value += scratch[idx + i*heightWidth] * kernel[i+offset];
                }
                for (; i <= offset; i++) {
                    value += scratch[idx + new_offset_pos*heightWidth] * kernel[i+offset];
                }
        
                image[idx] = value
            }
        }
    }

    return image
}

function getKernelSizeForSigma(sigma) {
    const MAX_KERNEL_SIZE = 4096
    var size = Math.ceil(sigma*6)
    if (size % 2 == 0) {
        size++
    }
    if (size < 3) {
        size = 3
    }
    if (size>=MAX_KERNEL_SIZE) {
        size=MAX_KERNEL_SIZE
    }
    return size
}

function fillGaussianBlurKernel1D(sigma, size, kernel) {
    const sigma2 = sigma * sigma
    const middle = Math.floor(size/2)
    for (var i=0; i<size; i++) {
        const distance = middle - i
        const distance2 = distance * distance
        const s = 1.0 / (sigma*Math.sqrt(2.0*Math.PI)) * Math.exp(-distance2/(2.0*sigma2))
        kernel[i] = s
    }
}